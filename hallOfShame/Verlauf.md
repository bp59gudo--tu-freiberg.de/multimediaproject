# Verlauf
## Fehler:
- broken_0: im Array, in dem die Vertices gebuffert wurden, gab es am Schluss noch ein paar 0en zu viel (nach array.resize(maxSize))
- broken_1: Bitmap als RGB eingelesen und nicht als RGBA
- broken_2: Indices wurden nicht genutzt
- broken_3: gleicher Fehler
