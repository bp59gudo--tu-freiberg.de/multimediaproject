#!/bin/bash

#install necessary packages
echo "install necessary packages:"
sudo apt install automake libtool autoconf

#download freetype
echo "downloading freetype:"
wget https://download.savannah.gnu.org/releases/freetype/freetype-2.10.0.tar.gz
tar -xzf freetype-2.10.0.tar.gz
cd freetype-2.10.0/

#install freetype
echo "building library:"
sh autogen.sh
make
sudo make install

#remove unnecessary files
echo "removing downloaded files"
cd ..
rm -r freetype-2.10.0
rm freetype-2.10.0.tar.gz

echo "installed freetype library in /usr/local"
