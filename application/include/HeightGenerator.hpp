#ifndef HEIGHTGENERATOR_HPP
#define HEIGHTGENERATOR_HPP

#include <cmath>
#include <random>
#include "util.hpp"
#include "GameSettings.hpp"

/**
 * @brief generates the height values for the terrain. Taken from: https://www.youtube.com/watch?v=qChQrNWU9Xw
 * 
 */
class HeightGenerator
{
private:
	///@param amplitude amplitude of terrain
	const float amplitude = GameSettings::instance()->getAmplitude();
	///@param octaves octaves/ generation layers of the generator
	const int octaves = GameSettings::instance()->getOctaves();
	///@param roughness roughness of the terrain
	const float roughness = GameSettings::instance()->getRoughness();

	int seed;
	int xOffset = 0;
	int zOffset = 0;

	/**
	 * @brief Get the noise based on the x and z coordinate
	 * 
	 * @param x position
	 * @param z position
	 * @return float noise value
	 */
	float getNoise(int x, int z);

	/**
	 * @brief Get smooth noise by interpolating between all neighbours
	 * 
	 * @param x position
	 * @param z position
	 * @return float smoothed noise
	 */
	float getSmoothNoise(int x, int z);

	/**
	 * @brief Get the interpolated noise at a position
	 * 
	 * @param x position
	 * @param z position
	 * @return float interpolated noise
	 */
	float getInterpolatedNoise(float x, float z);

public:
	/**
	 * @brief Construct a new Height Generator object
	 * 
	 */
	HeightGenerator();
	/**
	 * @brief Construct a new Height Generator object
	 * 
	 * @param gridX start x position in world coordinates
	 * @param gridZ start y position in world coordinates
	 * @param vertexCount length of an terrain axis
	 * @param seed seed of the RNG
	 */
	//only works with POSITIVE gridX and gridZ values!
	HeightGenerator(int gridX, int gridZ, int vertexCount, int seed);

	/**
	 * @brief Destroy the Height Generator object
	 * 
	 */
	~HeightGenerator() {}

	/**
	 * @brief interpolate between two values by factor b
	 * 
	 * @param a first value 
	 * @param b second value
	 * @param blend blend factor
	 * @return float interpolation
	 */
	float interpolate(float a, float b, float blend);

	/**
	 * @brief generate height for an position
	 * 
	 * @param x position
	 * @param z position
	 * @return float height
	 */
	float generateHeight(int x, int z);

	/**
	 * @brief get height of an (x,z) position. Is interpolated
	 * 
	 * @param x position
	 * @param z position
	 * @return float 
	 */
	float generateInterpolatedHeight(float x, float z);

	/**
	 * @brief return the max. amplitude for the terrain
	 * 
	 * @return float amplitude
	 */
	float getMaxAmplitude();
};

#endif