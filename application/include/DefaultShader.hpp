#ifndef DEFAULT_SHADER_HPP
#define DEFAULT_SHADER_HPP

#include "ShaderProgram.hpp"
#include "Model3D.hpp"
#include "glm/mat4x4.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "util.hpp"

/**
 * @brief default shader program for rendering items. Has to have the default uniform uniform_mvp
 * 
 */
class DefaultShader : public ShaderProgram
{
private:
    ///@param vertexPath path to the vertex shader file
    std::string vertexPath;
    ///@param fragmentPath path to the fragment shader file
    std::string fragmentPath;

    /**
     * @brief initialize the shader program
     * 
     */
    void initProgram() override;
public:
    /**
     * @brief Construct a new Default Shader
     * 
     * @param _vertexPath path the the vertex shader
     * @param _fragmentPath path to the fragment shader
     */
    DefaultShader(std::string _vertexPath = "vertex.glsl", std::string _fragmentPath = "fragment.glsl");

    /**
     * @brief Destroy the Default Shader
     * 
     */
    ~DefaultShader();
};

#endif