#ifndef GAME_STATE_HPP
#define GAME_STATE_HPP

#include "Event.hpp"
#include "Camera.hpp"
#include "GameUserData.hpp"

class Camera;
class RenderEngine;
class GameUserData;

/**
 * @brief Class for managing all game events and the state machine of the game.
 * 
 */
class GameState{
public:
   
   /**
    * @brief enum for possible game states
    * 
    */
    enum class State {running,menu,paused};
    //if more GameEvents are added, don't forget to add them in 'subscribeGameEvent' and 'unsubscribeGameEvent'!
    //if we need a lot of GameEvents, we might be better off saving them as <Event, GameEvent> tuples in a vector -> easier to manage
    /**
     * @brief enum for game events that are managed in GameState
     * 
     */
    enum class GameEvent {looseGame,winGame,pause,showOptions,resizeWindow}; 

    /**
     * @brief Construct a new Game State object. Since we only have one player camera, the gameCamera object is not supposed to change. In other games this might be a subject to change
     * 
     * @param _state initial game state
     * @param _gameCamera camera of the game
     */
    GameState(State _state)
     : state(_state) 
     {
		DEBUG_METHOD("calling GameState::GameState");         
     }

    /**
     * @brief Get the State object
     * 
     * @return State 
     */
    State getState();

    /**
     * @brief Fire an event
     * 
     * @param event 
     */
    void setEvent(GameEvent event);

    /**
     * @brief subscribe to an event with a void(int) function. This can't be a method
     * 
     * @param func function to subscribe with
     * @param event event to subscribe to
     */
    void subscribeGameEvent(std::function<void(int)> func, GameEvent event);

    /**
     * @brief unsubscribe from an event
     * 
     * @param func function to subscribe with
     * @param event event to subscribe to
     */
    void unsubscribeGameEvent(std::function<void(int)> func, GameEvent event);

    /**
     * @brief subscribe to an event with a void(Camera&) Method. This is supposed to be called from the game camera object.
     * 
     * @param func function to subscribe with
     * @param event event to subscribe to
     */
    void subscribeGameEvent(std::function<void(Camera&)> func, GameEvent event);

    /**
     * @brief unsubscribe from an event
     * 
     * @param func function to subscribe with
     * @param event event to subscribe to
     */
    void unsubscribeGameEvent(std::function<void(Camera&)> func, GameEvent event);

    /**
     * @brief subscribe to an event with a void(RenderEngine &) Method. This is supposed to be called from the RenderEngine object.
     * 
     * @param func function to subscribe with
     * @param event event to subscribe to
     */
    void subscribeGameEvent(std::function<void(RenderEngine&)> func, GameEvent event);

    /**
     * @brief unsubscribe from an event
     * 
     * @param func function to subscribe with
     * @param event event to subscribe to
     */
    void unsubscribeGameEvent(std::function<void(RenderEngine&)> func, GameEvent event);

private:
    State state;

    Event<void, RenderEngine&> eventLooseGame;
    Event<void, RenderEngine&> eventWinGame;
    Event<void, int> eventPauseGame;
    Event<void, int> eventShowOptions;
    Event<void, Camera&> eventResizeWindow;

    Camera* gameCamera;
    RenderEngine* renderEngine;

protected:
    /**
     * @brief initialize the reference to main camera and to the render engine
     * 
     * @param _gameCamera main camera
     * @param _renderEngine render engine
     */
    void initialize(Camera* _gameCamera, RenderEngine* _renderEngine);
    friend class GameUserData;
};

#endif