#ifndef TERRAIN_HPP
#define TERRAIN_HPP

#include "glm/vec3.hpp"
#include "glm/mat4x4.hpp"
#include "glm/gtx/transform.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "GameObject.hpp"
#include "HeightGenerator.hpp"
#include "Camera.hpp"
#include "GameSettings.hpp"

/**
 * @brief terrain class
 * 
 */
class Terrain : public RenderObject
{
private:
	//GLint texLoc;
	GLint modelLoc, viewLoc, projectionLoc;
	Model3D *model;
	glm::mat4 modelMatrix;
	glm::vec3 worldPosition;
	float SIZE = GameSettings::instance()->getterrainSize();
	int VERTEX_COUNT = GameSettings::instance()->getVertexCount();
	float xcoord;
	float zcoord;
	HeightGenerator generator;
	Camera *camReference;
	std::vector<float> heights;
	/**
     * @brief Get height(y-coordinate) of terrain vertices
     * 
     * @param x coordinate
     * 
     * @param z coordinate
     * 
     * @return float 
     */
	float getHeight(int x, int z);

	/**
	 * @brief barycentric interpolation -> for interpolating between height values of a triangle
	 * 
	 * @param p1 point 1 of triangle
	 * @param p2 point 2 of triangle
	 * @param p3 point 3 of triangle
	 * @param pos x and z position in world coordinates
	 * @return float interpolated height
	 */
	float barycentricInterpolation(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos);
public:

	/**
	 * @brief Construct a new Terrain object
	 * 
	 * @param gameCamera main camera
	 * @param _shader shader
	 * @param _position world position
	 * @param _scale scale
	 */
	Terrain(Camera *gameCamera, ShaderProgram *_shader, glm::vec3 _position = glm::vec3(0.0f, 0.0f, 0.0f),
			float _scale = 1.0f) : camReference(gameCamera), generator(HeightGenerator()), xcoord(_position.x), zcoord(_position.z), RenderObject(_shader)
	{
		DEBUG_METHOD("calling Terrain::Terrain");
		modelMatrix = glm::mat4(1.0f);

		//translate model
		modelMatrix = glm::translate(modelMatrix, _position);
		worldPosition = glm::vec3(modelMatrix[3][0], modelMatrix[3][1], modelMatrix[3][2]);

		//scale model
		modelMatrix = glm::scale(modelMatrix, glm::vec3(_scale, _scale, _scale));

		generateTerrain(VERTEX_COUNT, SIZE);

		//get uniform locations
		modelLoc = program->getUniformLoc("uniform_model");
		checkError(modelLoc < 0, "Failed to obtain uniform location for uniform_model");
		viewLoc = program->getUniformLoc("uniform_view");
		checkError(viewLoc < 0, "Failed to obtain uniform location for uniform_view");
		projectionLoc = program->getUniformLoc("uniform_projection");
		checkError(projectionLoc < 0, "Failed to obtain uniform location for uniform_projection");
	}

	/**
	 * @brief Destroy the Terrain object
	 * 
	 */
	~Terrain()
	{
		DEBUG_METHOD("calling Terrain::~Terrain");
	}

	/**
     * @brief Get the x coordinate
     * 
     * @return float 
     */
	float getX();

	/**
     * @brief Get the z coordinate
     * 
     * @return float 
     */
	float getZ();

	/**
     * @brief Get the x coordinate
     * 
     * @return float 
     */
	Model3D *getModel3D() const;

	/**
     * @brief Get size of terrain in x/z length
     * 
     * @return int 
     */
	int getSIZE();

	/**
     * @brief Get vertex_count in x/z length
     * 
     * @return float 
     */
	float getVERTEX_COUNT();

	/**
     * @brief Get height(y-coordinate) of terrain for game actor
     * 
     * @param worldX coordinate
     * 
     * @param worldZ coordinate
     * 
     * @return float 
     */
	float getHeightOfTerrain(float worldX, float worldZ, bool &passedTerrainBorder);

	/**
	 * @brief using height generator to generate the y position (height) for a x and z position
	 * 
	 * @param x position
	 * @param z position
	 * @return float 
	 */
	float generateHeight(float x, float z);

	/**
    * @brief calculate normal for 3 vertices vectors
    * 
    * @return vec3
    */
	inline glm::vec3 calcNormal(int x, int z);

	/**
     * @brief generate 3D terrain
     * 
     * @param Terrain generated with HeightGenerator
     * @return Model3D
     */
	void generateTerrain(int vertexCount, int size);

	/**
	 * @brief render terrain
	 * 
	 */
	void render() override;
};

#endif