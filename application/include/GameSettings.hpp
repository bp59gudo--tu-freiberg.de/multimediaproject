#ifndef GAME_SETTINGS_HPP
#define GAME_SETTINGS_HPP

#include <iostream>
#include <err.h>
#include "util.hpp"

/**
 * @brief manages all important game settings
 * 
 */
class GameSettings{
private:
    static GameSettings* gsInstance;

    //settings for Player
    static std::string playerName;
    static bool fullscreen;
    static float mouseSensivity;

    //settings for Bots:
    static int MaxMinAngle;
    static int mintimeNextMovement;
    static int maxtimeNextMovement;
    static int amountGameActors;
    static int amountItems;

    //settings for terrain:
    static float mountainEdge;
    static float terrainSize;
    static int vertexCount;

    //settings for heightgenerator:
    static float amplitude;
	static int octaves;
	static float roughness;

    //settings for gameactor:
    static float speed;

    /**
     * @brief Construct a new Game Settings object
     * 
     */
    GameSettings();

    /**
     * @brief Destroy the Game Settings object
     * 
     */
    ~GameSettings();

public:
     /**
     * @brief use this to access the singleton instance -> e.g.: GameSettings::instance()->mintimeNextMovement;
     * 
     * @return GameSettings* pointer to the instance
     */
    static GameSettings* instance();

    /**
     * @brief check if settings specify fullscreen or not
     * 
     * @return true when fullscreen
     * @return false when not fullscreen
     */
    static bool getFullscreen();

    /**
     * @brief Mouse Sensivity of player camera
     * 
     * @return float
     */
    static float getSensivity();
    
    /**
     * @brief maximum steering goal (in degrees) to steer bot to the left or right
     * 
     * @return int
     */
    static int getMaxMinAngle();
     /**
     * @brief minimum time (in seconds) where bot has to reach its steering goal 
     * 
     * @return int
     */
    static int getMinTimeMovement();

     /**
     * @brief maximum time (in seconds) where bot has to reach its steering goal 
     * 
     * @return int
     */
    static int getMaxTimeMovement();

    /**
     * @brief amount of bots who will be created
     * 
     * @return int
     */
    static int getAmountGameactors();
    /**
     * @brief amount of items which will be created 
     * 
     * @return int
     */
    static int getAmountItems();

    /**
     * @brief float value which will be added onto the vertices height of the edge
     * 
     * @return float
     */
    static float getMountainEdge();

    /**
     * @brief terrain size in x and z direction
     * 
     * @return float
     */
    static float getterrainSize();

    /**
     * @brief vertices of terrain in x and z direction
     * 
     * @return int
     */
    static int getVertexCount();

    /**
     * @brief maximum amplitude of terrain
     * 
     * @return float
     */
    static float getAmplitude();

    /**
     * @brief number of octaves of the terrain
     * 
     * @return int
     */
    static int getOctaves();

    /**
     * @brief  roughness of the terrain
     * 
     * @return float
     */
    static float getRoughness();

     /**
     * @brief  speed of movement
     * 
     * @return float
     */
    static float getSpeed();

    /**
     * @brief Get the name of the player
     * 
     * @return std::string 
     */
    static std::string getPlayerName();

    /**
     * @brief read the settings from an file
     * 
     * @param fileName path to file
     */
    static void loadSettingsFile(std::string fileName);

};


#endif