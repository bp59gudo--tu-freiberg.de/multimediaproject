#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "GameActor.hpp"

void mouseCallback(GLFWwindow *window, double xpos, double ypos);

/**
 * @brief player class for the game
 * 
 */
class Player : public GameActor
{
private:
    /**
     * @brief updates camera position (called once in a frame)
     * 
     */
    void updateCamera();

protected:
    friend void mouseCallback(GLFWwindow *window, double xpos, double ypos);

    /**
     * @param lastX last x position of mouse on screen
     * @param lastY last y position of mouse on screen
     * @param yaw yaw of the camera look angle
     * @param firstMouse if this is the first frame, this is set to true
     */
    float lastX, lastY, yaw;
    bool firstMouse;

    /**
     * @brief change look direction
     * 
     * @param angle angle for new look direction
     */
    void changeDirection(float angle) override;

    /**
     * @brief Set the look direction
     * 
     * @param dir new look direction
     */
    void setDirection(glm::vec3 dir);

public:
    /**
     * @brief Construct a new Player object
     * 
     * @param _name player name
     * @param _model model of the player
     * @param _shader shader
     * @param _position world position
     * @param _scale scale
     */
    Player(std::string _name, Model3D *_model, ShaderProgram *_shader, glm::vec3 _position = glm::vec3(50.0f, 0.0f, 50.0f),
           float _scale = 1.0f) : GameActor(_name, _model, _shader, glm::vec3(0.0f, 0.0f, 0.0f), _position, _scale)
    {
		DEBUG_METHOD("calling Player::Player");
        yaw = 0.0f;
    }

    /**
     * @brief Destroy the Player object
     * 
     */
    ~Player()
    {
		DEBUG_METHOD("calling Player::~Player");
    }

    /**
     * @brief render the player
     * 
     */
    void render();

    /**
     * @brief Get look direction
     * 
     * @return glm::vec3 
     */
    glm::vec3 getDirection();
};

#endif