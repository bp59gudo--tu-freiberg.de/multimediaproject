#ifndef SKYBOX_HPP
#define SKYBOX_HPP

#include "GameObject.hpp"
#include "Camera.hpp"
#include "math.h"

/**
 * @brief class for fake-skybox
 * 
 */
class Skybox : public GameObject
{
protected:
    GLint texLoc;
    GLint modelLoc, viewLoc, projectionLoc;

public:
    /**
     * @brief Construct a new skybox object
     * 
     * @param _model model for the skybox
     * @param _shader shader to render the model
     * @param _position position in world coordinates
     * @param _scale scale of the model
     */
    Skybox(Model3D *_model, ShaderProgram *_shader, glm::vec3 _position = glm::vec3(0.0f, 0.0f, 0.0f),
           float _scale = 1.0f) : GameObject(_model, _shader, _position, _scale)
    {
        DEBUG_METHOD("calling Skybox::Skybox");

        // get uniform locations
        texLoc = program->getUniformLoc("textureSampler");
        checkError(texLoc < 0, "Failed to obtain uniform location for textureSampler");
        modelLoc = program->getUniformLoc("uniform_model");
        checkError(modelLoc < 0, "Failed to obtain uniform location for uniform_model");
        viewLoc = program->getUniformLoc("uniform_view");
        checkError(viewLoc < 0, "Failed to obtain uniform location for uniform_view");
        projectionLoc = program->getUniformLoc("uniform_projection");
        checkError(projectionLoc < 0, "Failed to obtain uniform location for uniform_projection");
    }

    /**
     * @brief Destroy the Skybox object
     * 
     */
    ~Skybox()
    {
        DEBUG_METHOD("calling Skybox::~Skybox");
    }

    /**
     * @brief Get the Object Type
     * 
     * @return ObjectType 
     */
    ObjectType getObjectType() override;

    /**
     * @brief render the skybox
     * 
     */
    virtual void render() override;

    /**
     * @brief called when colliding with another PhysicsObject
     * 
     * @param other 
     */
    void collide(PhysicsObject *other) override;

    /**
     * @brief Get the size
     * 
     * @return float 
     */
    float getSize();

    /**
     * @brief rotate skybox by rotation angle
     * 
     * @param angle rotation angle
     */
    void rotate(float angle);

    /**
     * @brief Get the shader of the skybox
     * 
     * @return ShaderProgram* 
     */
    ShaderProgram* getShaderProgram();
};

#endif