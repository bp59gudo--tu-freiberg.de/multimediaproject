#ifndef GAMEACTOR_HPP
#define GAMEACTOR_HPP

#include "GameObject.hpp"
#include "Camera.hpp"
#include "math.h"
#include "GameSettings.hpp"

class Camera;
class Item;

/**
 * @brief base class for game actors (bots and the player)
 * 
 */
class GameActor : public GameObject
{
protected:
    GLint texLoc;
    GLint modelLoc, viewLoc, projectionLoc, colorLoc;
    float newColorR, newColorG, newColorB;
    glm::vec3 lookDirection;
    std::vector<float> color;
    std::string name;
    float currentDirection = 0.0f;
    bool negativeSteering = true;
    float timeNextMovement = 0.0f;
    float goalOflookDirection = 0.0f;
    int mintimeNextMovement;
    int maxtimeNextMovement;
    float visualScale;

    static std::vector<GameActor *> *otherActors;

    /**
     * @brief change look direction (does not rotate object)
     * 
     * @param angle angle in RAD
     */
    virtual void changeDirection(float angle);

    /**
     * @brief move the GameActor by a vec2 (x and z delta)
     * 
     * @param deltaMovement delta vector -> new pos = old pos + deltaMovement
     */
    void move(glm::vec2 deltaMovement);

public:
    /**
     * @brief Construct a new Game Actor object
     * 
     * @param _name name for this Game Actor
     * @param _model model for the game object
     * @param _shader shader to render the model
     * @param actorColor color of the actor
     * @param _position position in world coordinates
     * @param _scale scale of the model
     */
    GameActor(std::string _name, Model3D *_model, ShaderProgram *_shader, glm::vec3 actorColor, glm::vec3 _position = glm::vec3(0.0f, 0.0f, 0.0f),
              float _scale = 1.0f) : GameObject(_model, _shader, _position, _scale), name(_name)
    {
        DEBUG_METHOD("calling GameActor::GameActor");

        //init look direction
        lookDirection = glm::vec3(1.0f, 0.0f, 0.0f);

        //if model has a texture -> get shader location of the texture sampler
        if (_model->hasTexture())
        {
            // Texture sampler:
            texLoc = program->getUniformLoc("textureSampler");
            checkError(texLoc < 0, "Failed to obtain uniform location for textureSampler");
        }
        else
        {
            texLoc = 0;
        }

        //random time (in seconds) where the yaw has te be reached
        mintimeNextMovement = GameSettings::instance()->getMinTimeMovement() * 10;
        maxtimeNextMovement = GameSettings::instance()->getMaxTimeMovement() * 10;

        visualScale = scale;

        //set color
        color = std::vector<float>();
        color.emplace_back(actorColor.x);
        color.emplace_back(actorColor.y);
        color.emplace_back(actorColor.z);

        //get uniform locations
        colorLoc = program->getUniformLoc("uniform_color");
        checkError(colorLoc < 0, "Failed to obtain uniform location for uniform_color");
        modelLoc = program->getUniformLoc("uniform_model");
        checkError(modelLoc < 0, "Failed to obtain uniform location for uniform_model");
        viewLoc = program->getUniformLoc("uniform_view");
        checkError(viewLoc < 0, "Failed to obtain uniform location for uniform_view");
        projectionLoc = program->getUniformLoc("uniform_projection");
        checkError(projectionLoc < 0, "Failed to obtain uniform location for uniform_projection");

        //set collision radius to own scale
        collisionRadius = scale;
    }

    /**
     * @brief Destroy the Game Actor object
     * 
     */
    ~GameActor()
    {
        DEBUG_METHOD("calling GameActor::~GameActor");
    }

    /**
     * @brief Get the Object Type
     * 
     * @return ObjectType 
     */
    ObjectType getObjectType() override;

    /**
     * @brief render the actor
     * 
     */
    virtual void render() override;

    /**
     * @brief this is called when the actor collides with another physics object
     * 
     * @param other other phsysics object (Item or GameActor)
     */
    void collide(PhysicsObject *other) override;

    /**
     * @brief Get the Size object
     * 
     * @return float size
     */
    float getSize();

    /**
     * @brief Get the Name of the actor
     * 
     * @return std::string name
     */
    std::string getName();

    /**
     * @brief Set the Color of the actor
     * 
     * @param red red ratio [0.0, 1.0] 
     * @param green green ratio [0.0, 1.0] 
     * @param blue blue ratio [0.0, 1.0] 
     */
    void setColor(float red, float green, float blue);

    /**
     * @brief rotate actor
     * 
     * @param angle rotation angle
     */
    void rotate(float angle);

    /**
     * @brief move the actor (should be called once in a frame)
     * 
     */
    void move();

    /**
     * @brief move actor randomly
     * 
     */
    void randomMovement();

    /**
     * @brief SMART movement
     * 
     */
    void findEatable();

    /**
     * @brief Set a pointer to all other game objects in the scene (in RenderEngine)
     * 
     * @param others all gameobjects in scene
     */
    static void setOtherObjects(std::vector<GameActor *> *others);

    /**
     * @brief Get the pointer to the shader program
     * 
     * @return ShaderProgram* 
     */
    ShaderProgram *getShaderProgram();
};

#endif