#ifndef UI_FONT_HPP
#define UI_FONT_HPP

#include "util.hpp"
#include <math.h>
#include "glm/vec3.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "Camera.hpp"
#include "DefaultShader.hpp"

#include <experimental/filesystem>
#include <map>

#include "ft2build.h"
#include FT_FREETYPE_H

class Camera;

/**
 * @brief class to render text on UI or at a world position. Based on https://learnopengl.com/In-Practice/Text-Rendering
 * 
 */
class Font
{
private:
    typedef struct
    {
        unsigned int textureID; // ID handle of the glyph texture
        glm::ivec2 size;        // Size of glyph
        glm::ivec2 bearing;     // Offset from baseline to left/top of glyph
        unsigned int advance;   // Offset to advance to next glyph
    } Character;
    FT_Library ftLib;
    static Camera *camRef;
    FT_Face face; //=font
    std::map<char, Character> characters;
    GLuint vao, vbo;
    GLuint texture;
    glm::mat4 textOrientation;

public:
    /**
    * @brief Construct a new Font object
    * 
    * @param fontPath path to the true type font
    * @param fontSizeHeight font height
    * @param fontSizeWidth font width
    */
    Font(std::string fontPath, int fontSizeHeight, int fontSizeWidth = 0);

    /**
     * @brief Destroy the Font object
     * 
     */
    ~Font();

    /**
     * @brief render a text on screen at a position
     * 
     * @param text text to render
     * @param shader shader to render the font
     * @param xPos x position on screen
     * @param yPos y position on screen
     * @param scale scale of the font
     * @param color color of the font
     */
    void renderUIText(std::string text, DefaultShader *shader, float xPos, float yPos, float scale, glm::vec3 color);

    /**
     * @brief render a text at a world position
     * 
     * @param text text to render
     * @param shader shader to render
     * @param worldPosition position in world
     * @param scale scale
     * @param color color
     */
    void renderPositionText(std::string text, DefaultShader *shader, glm::vec3 worldPosition, float scale, glm::vec3 color);

    /**
     * @brief initialize the camera for all font objects. Needs to be called before rendering a positionText
     * 
     * @param gameCamera main camera
     */
    static void initialize(Camera *gameCamera);
};

#endif