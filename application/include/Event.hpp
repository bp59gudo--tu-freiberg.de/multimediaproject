#ifndef EVENT_HPP
#define EVENT_HPP

#include <vector>
#include <functional>
#include "util.hpp"

//for reference: https://en.cppreference.com/w/cpp/utility/functional/function/target and
//https://stackoverflow.com/questions/20833453/comparing-stdfunctions-for-equality (C++ can get ugly)
/**
 * @brief Get the function address of an function wrapped in a std::function
 * 
 * @tparam returnValue return value of the std::function
 * @tparam args argument types of the std::function
 * @param function the std::function
 * @return size_t pointer to address
 */
template<typename returnValue, typename... args>
size_t getAddress(std::function<returnValue(args...)> function) {
    //typedef of a function pointer -> fnType
    typedef returnValue(fnType)(args...);
    fnType ** fnPointer = function.template target<fnType*>();
    return (size_t) *fnPointer;
}

/**
 * @brief checks if two std::functions wrap the same function
 * 
 * @tparam returnValue type of the return value
 * @tparam args types of the arguments
 * @param first first std::function
 * @param second second std::function
 * @return true if equal
 * @return false if not equal
 */
template<typename returnValue, typename... args>
bool operator==(std::function<returnValue(args...)> first, std::function<returnValue(args...)> second)
{
    return getAddress(first) == getAddress(second);
}

/**
 * @brief event implementation. This implementation is pretty horrible in C++, because there is for example no way to pass both functions + methods and void + other parameters.
 *  Therefore you always need to have a parameter.
 * 
 * @tparam returnValue type of return value of subscribed functions
 * @tparam args argument types of subscribed functions
 */
template<typename returnValue, typename... args>
class Event
{
private:
    std::vector<std::function<returnValue(args...)>> subscribedFunctions;

    bool isFunctionSubscribed(std::function<returnValue(args...)> func)
    {
		DEBUG_METHOD("calling Event::isFunctionSubscribed");
        bool found = false;
        for(auto& f : subscribedFunctions)
        {
            if(f == func)
            {
                found = true;
                break;
            }
        }
        return found;
    }
public:
    Event()
    {
	    DEBUG_METHOD("calling Event::Event");
        subscribedFunctions = std::vector<std::function<returnValue(args...)>>();
    }
    ~Event()
    {
		DEBUG_METHOD("calling Event::~Event");
        subscribedFunctions.clear();
    }

    /**
     * @brief call this event
     * 
     */
    void fire(args... arguments)
    {
		DEBUG_METHOD("calling Event::fire");
        for(auto& func : subscribedFunctions)
        {
            func(arguments...);
        }
    }

    /**
     * @brief subscribe with a function pointer of type 'functionPtr'
     * 
     * @param func function pointer
     */
    void subscribe(std::function<returnValue(args...)> func)
    {
		DEBUG_METHOD("calling Event::subscribe");
        if(!isFunctionSubscribed(func))
        {
            subscribedFunctions.emplace_back(func);
        }
    }

    /**
     * @brief unsubscribe a function which was subscribed previously
     * 
     * @param func function pointer
     */
    void unsubscribe(std::function<returnValue(args...)> func)
    {
		DEBUG_METHOD("calling Event::unsubscribe");
        auto iterator = subscribedFunctions.begin();
        while(iterator != subscribedFunctions.end())
        {
            if(func == *iterator)
            {
                subscribedFunctions.erase(iterator);
                break;
            }
            iterator++;
        }
    }
};

#endif