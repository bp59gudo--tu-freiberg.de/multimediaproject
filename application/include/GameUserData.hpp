#ifndef GAME_ENGINE_HPP
#define GAME_ENGINE_HPP

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include "RenderEngine.hpp"
#include "PhysicsEngine.hpp"
#include "GameState.hpp"
#include "Terrain.hpp"
#include "DefaultShader.hpp"
#include "Camera.hpp"
#include "Font.hpp"
#include <thread>

int main();
void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);
class Camera;
class GameState;
class GameObject;
class GameActor;
class Player;

class RenderEngine;

//using singleton pattern (see https://de.wikibooks.org/wiki/C%2B%2B-Programmierung:_Entwurfsmuster:_Singleton)
/**
 * @brief singleton class managing access to all important parts of the game
 * 
 */
class GameUserData
{
private:
    static GameUserData* gudInstance;

    static int windowWidth;
    static int windowHeight;
    static double dTime, lastTime;

    static RenderEngine* renderEngine;
    static PhysicsEngine* physicsEngine;
    static GameState* gameState;
    static Terrain* terrain;
    static GLFWwindow* window;
    static Camera* gameCamera;
    static DefaultShader* terrainShader;
    static DefaultShader* skyboxShader;
    static Player* player;

    static std::vector<GameObject*> toDelete;

    static bool cameraFlight;

    /**
     * @brief Construct a new Game User Data object
     * 
     */
    GameUserData();

    /**
     * @brief Destroy the Game User Data object
     * 
     */
    ~GameUserData();

    /**
     * @brief called at the end of the frame. Removes all objects that are in the 'toDelete' vector
     * 
     */
    static void removeGameObjects();

protected:
    friend int main();
    friend void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);

    //needs to be called in main function
    /**
     * @brief initialize the GameUserData
     * 
     * @param wndPtr pointer to the window
     * @param player pointer to the player
     * @param wndWidth window width
     * @param wndHeight window height
     */
    static void initialize(GLFWwindow* wndPtr, Player* player, int wndWidth, int wndHeight);

    /**
     * @brief update the game -> called each frame
     * 
     */
    static void update();

    /**
     * @brief Get the time since the last frame
     * 
     * @return double 
     */
    static double getDeltaTime();

public:
    /**
     * @brief use this to access the singleton instance -> e.g.: GameUserData::instance()->getTerrain();
     * 
     * @return GameUserData* pointer to the instance
     */
    static GameUserData* instance();

    /**
     * @brief Get the Game State object
     * 
     * @return GameState* 
     */
    static GameState* getGameState();

    /**
     * @brief Get the Terrain object
     * 
     * @return Terrain* 
     */
    static Terrain* getTerrain();

    /**
     * @brief Get the Game Camera
     * 
     * @return Camera* 
     */
    Camera* getGameCamera();

    /**
     * @brief Get the Window Pointer object
     * 
     * @return GLFWwindow* 
     */
    static GLFWwindow* getWindowPointer(void);

    /**
     * @brief Get the Window Width
     * 
     * @return int 
     */
    static int getWindowWidth();

    /**
     * @brief Get the Window Height
     * 
     * @return int 
     */
    static int getWindowHeight();

    /**
     * @brief resize the window
     * 
     * @param width new width
     * @param height new height
     */
    static void resizeWindow(int width, int height);

    /**
     * @brief add a new game object to the scene renderEngine and physicsEngine
     * 
     * @param obj new game object
     */
    static void addGameObject(GameObject* obj);

    /**
     * @brief remove a game object from the scene
     * 
     * @param obj object
     */
    static void removeGameObject(GameObject *obj);

    /**
     * @brief updates the scoreboard (called when points of an actor change)
     * 
     */
    static void updateScoreboard();

    /**
     * @brief Get a reference to the player
     * 
     * @return Player* 
     */
    static Player* getPlayer();

    /**
     * @brief check if camera flight is enabled (only possible when debugging enabled)
     * 
     * @return true is enabled
     * @return false is disabled
     */
    static bool getCameraFlight();

    /**
     * @brief toggle camera flight mode
     * 
     */
    static void toggleCameraFlight();

    /**
     * @brief Set camera flight mode to true or false
     * 
     * @param camFlight 
     */
    static void setCameraFlight(bool camFlight);
};

#endif