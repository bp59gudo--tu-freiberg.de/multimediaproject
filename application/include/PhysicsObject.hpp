#ifndef PHYSICS_OBJECT_HPP
#define PHYSICS_OBJECT_HPP

#include <vector>
#include <algorithm>

#include "glm/vec2.hpp"
#include "glm/geometric.hpp"
#include "util.hpp"

/**
 * @brief Base class for game objects with a physics component
 * 
 */
class PhysicsObject
{
private:
    unsigned physicsID;
    static unsigned physicsObjectsAmount;
    friend bool operator==(const PhysicsObject &first, const PhysicsObject &second);
    friend bool operator!=(const PhysicsObject &first, const PhysicsObject &second);

protected:
    float collisionRadius;
    glm::vec2 center2D;
    std::vector<unsigned> collidedWithinFrame;

    /**
     * @brief Construct a new Physics Object object
     * 
     * @param _collisionRadius radius around the center -> for collision detection
     * @param _center2D center of the object in world coordinates
     */
    //set to protected, so it is not possible to create an instance of PhysicsObject (only instances of derived classes)
    PhysicsObject(float _collisionRadius, glm::vec2 _center2D) : collisionRadius(_collisionRadius), center2D(_center2D), 
        collidedWithinFrame(std::vector<unsigned>()), physicsID(physicsObjectsAmount++)
        {    
		DEBUG_METHOD("calling PhysicsObject::PhysicsObject");        
        }

public:
    /**
     * @brief Destroy the Physics Object object
     * 
     */
    ~PhysicsObject();

    /**
     * @brief updates the physics for a time delta. If this object collides with other physicsObjects during that time, 'collide' is called.
     * 
     * @param timeDelta passed time
     * @param objects vector of all physicsObjects where a collision check is necessary
     */
    void updatePhysics(float timeDelta, const std::vector<PhysicsObject *> &objects);

    /**
     * @brief collide with another object. IMPORTANT: the check is done in 'updatePhysics', not in collide!
     * When two PhysicsObjects collide, they add each other to 'collidedWithinFrame'. Derived classes need to add further logic
     * 
     * @param other PhysicsObject that collided with this object
     */
    virtual void collide(PhysicsObject *other) = 0;

    /**
     * @brief Get the Physics ID
     * 
     * @return unsigned 
     */
    unsigned getPhysicsID();

    /**
     * @brief resets 'collidedWithinFrame', needs to be called after each frame
     * 
     */
    virtual void resetCollision();
};

/**
 * @brief equals operator for two derivatives of PhysicsObjects
 * 
 * @param first first object
 * @param second second object
 * @return true if equal
 * @return false if not equal
 */
bool operator==(const PhysicsObject &first, const PhysicsObject &second);

/**
 * @brief unequals operator for two derivatives of PhysicsObjects
 * 
 * @param first first object
 * @param second second object
 * @return true if not equal
 * @return false if equal
 */
bool operator!=(const PhysicsObject &first, const PhysicsObject &second);

#endif