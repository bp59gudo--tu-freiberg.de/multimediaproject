#ifndef MODEL3D_HPP
#define MODEL3D_HPP

#define ATTRIB_POSITION 0
#define ATTRIB_COLOR 1
#define ATTRIB_NORMAL 2
#define ATTRIB_TEX_COORDS 3
#define ATTRIB_INDEX 4

#include "bitmap.h"
#include "util.hpp"
#include "glm/vec4.hpp"
#include "glm/vec3.hpp"
#include "glm/vec2.hpp"
#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include <vector>
#include <string>
#include <fstream>
#include <ios>
#include <sstream>
#include <iostream>
#include <tuple>
#include <experimental/filesystem>

/**
 * @brief class for our internel raw 3D models.
 * Based on obj.h from lecture and this video: https://www.youtube.com/watch?v=YKFYtekgnP8&list=PLRIWtICgwaX0u7Rf9zkZhLoLuZVfUksDP&index=10
 * 
 */
class Model3D
{
public:
    typedef struct
    {
        glm::vec3 ambient;
        glm::vec3 diffuse;
        glm::vec3 specular;
        float shininess;
    } Material;

    /**
     * @brief Construct a new Model 3D object from an .obj file
     * 
     * @param objFilePath path to .obj file
     * @param texturePath path to texture
     */
    Model3D(std::string objFilePath, std::string texturePath, Material &mat);

    /**
     * @brief Construct a new Model 3D object
     * 
     * @param positions vertices positions
     * @param mat model material
     * @param indices vertex indices
     * @param colors vertices colors
     * @param normals vertices normals
     * @param texCoords vertices texture coordinates
     * @param texturePath path to the texture
     */
    Model3D(std::vector<float> *positions, Material &mat, std::vector<int> *indices = nullptr, std::vector<unsigned char> *colors = nullptr,
            std::vector<float> *normals = nullptr, std::vector<float> *texCoords = nullptr, std::string texturePath = "");

    /**
     * @brief Construct a new Model 3D object
     * 
     * @param objFilePath path to the .obj file
     * @param colorR red color fragment
     * @param colorG green color fragment
     * @param colorB blue color fragment
     * @param mat material of the object
     */
    Model3D(std::string objFilePath, unsigned char colorR, unsigned char colorG, unsigned char colorB, Material &mat);

    /**
     * @brief Destroy the Model 3D object
     * 
     */
    ~Model3D();

    /**
     * @brief return a pointer to the vao
     * 
     * @return GLuint 
     */
    GLuint getVAO() const;

    /**
     * @brief Get the Texture ID pointer
     * 
     * @return GLuint 
     */
    GLuint getTextureID() const;

    /**
     * @brief bind the vao
     * 
     */
    void bindVAO();

    /**
     * @brief unbind the vao
     * 
     */
    void unbindVAO();

    /**
     * @brief Get the Vertex Count
     * 
     * @return unsigned 
     */
    unsigned getVertexCount();

    /**
     * @brief check if model is colored
     * 
     * @return true is colored
     * @return false is not colored
     */
    bool hasColor();

    /**
     * @brief check if model has normal vectors
     * 
     * @return true has normal vectors
     * @return false has no normal vectors
     */
    bool hasNormal();

    /**
     * @brief check if model is textured
     * 
     * @return true is textured
     * @return false is not textured
     */
    bool hasTexture();

private:
    GLuint vaoPtr;
    std::vector<std::tuple<int, GLuint>> vboPtrList;
    unsigned vertexDataCount;
    GLuint texturePtr;
    bool hasColors, hasNormals, hasTextureCoords;
    Material modelMaterial;

    typedef struct
    {
        GLfloat position[3];
        GLubyte color[3];
        GLfloat normal[3];
        GLuint index[3];
        GLfloat texCoord[2];
    } VertexData;

    //based on lecture
    typedef enum __obj_entry_type_t__
    {
        OBJ_ENTRY_TYPE_END,
        OBJ_ENTRY_TYPE_VERTEX,
        OBJ_ENTRY_TYPE_TEX_COORDS,
        OBJ_ENTRY_TYPE_NORMAL,
        OBJ_ENTRY_TYPE_FACE,
        OBJ_ENTRY_TYPE_MTL_IMPORT,
        OBJ_ENTRY_TYPE_MTL_USE
    } ObjEntryType_t;

    // An obj vertex entry:
    typedef struct __obj_vertex_entry_t__
    {
        double x;
        double y;
        double z;

        // Negative if not present:
        double w;
    } ObjVertexEntry_t;

    // An obj texture coordinates entry:
    typedef struct __obj_tex_coords_entry_t__
    {
        double u;
        double v;
    } ObjTexCoordsEntry_t;

    // An obj normal entry:
    typedef struct __obj_normal_entry_t__
    {
        double x;
        double y;
        double z;
    } ObjNormalEntry_t;

    // An obj face index triple:
    typedef struct __obj_face_index_triple_t__
    {
        // Missing indices are negative.
        int vertexIndex;
        int texCoordsIndex;
        int normalIndex;
    } ObjFaceIndexTriple_t;

    // An obj face entry:
    typedef struct __obj_face_entry_t__
    {
        ObjFaceIndexTriple_t triples[3];
    } ObjFaceEntry_t;

    // An obj mtl entry:
    typedef struct __obj_mtl_entry_t__
    {
        char mtlName[2048];
    } ObjMtlEntry_t;

    // An obj entry:
    typedef union _obj_entry_t_
    {
        ObjVertexEntry_t vertexEntry;
        ObjTexCoordsEntry_t texCoordsEntry;
        ObjNormalEntry_t normalEntry;
        ObjFaceEntry_t faceEntry;
        ObjMtlEntry_t mtlEntry;
    } ObjEntry_t;

    /**
     * @brief initializes a Model3D object
     * 
     * @param positions vertices positions
     * @param mat model material
     * @param indices vertex indices
     * @param colors vertices colors
     * @param normals vertices normals
     * @param texCoords vertices texture coordinates
     * @param texturePath path to the texture
     */
    void initialize(std::vector<float> *positions, Material &mat, std::vector<int> *indices, std::vector<unsigned char> *colors = nullptr,
                    std::vector<float> *normals = nullptr, std::vector<float> *texCoords = nullptr, std::string texturePath = "");

    /**
     * @brief counts entries of an .obj file
     * 
     * @param objFile filestream to .obj file
     * @param vertexCount (out) amount of vertices
     * @param texCoordsCount (out) amount of texture coordinates
     * @param normalCount (out) amount of normals
     * @param faceCount (out) amount of faces
     * @param mtlLibCount (out) amount of mtlLib
     */
    void objCountEntries(std::ifstream &objFile, int &vertexCount, int &texCoordsCount, int &normalCount, int &faceCount, int &mtlLibCount);

    /**
     * @brief get the next .obj entry
     * 
     * @param objFile filestream to .obj file
     * @param entry next entry
     * @return ObjEntryType_t type of the next entry
     */
    ObjEntryType_t objGetNextEntry(std::ifstream &objFile, ObjEntry_t *entry);

    /**
     * @brief bind the indices buffer
     * 
     * @param indices pointer to the indices data
     * @param size size of indices array
     */
    void bindIndicesBuffer(int *indices, GLsizeiptr size);

    /**
     * @brief store attribute data in an vbo
     * 
     * @tparam type of the data
     * @param attribNumber number of the attribute (use the defined ATTRIB_...)
     * @param data pointer to the data
     * @param numberOfComponents number of components in each data element (e.g. position with x,y,z has three)
     * @param size size of the data array
     * @param normalized should the data be normalized
     */
    template <typename t>
    void storeDataInAttribList(int attribNumber, t *data, GLenum dataType, GLint numberOfComponents, GLint size, GLboolean normalized);

    /**
     * @brief Get the Material of the model
     * 
     * @return Material 
     */
    Material getMaterial();
};

#endif