#ifndef RENDER_ENGINE_HPP
#define RENDER_ENGINE_HPP

#include "Engine.hpp"
#include "util.hpp"
#include "Camera.hpp"
#include "GameActor.hpp"
#include "GameObject.hpp"
#include "Item.hpp"
#include "Terrain.hpp"
#include "DefaultShader.hpp"
#include "Skybox.hpp"
#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include <vector>
#include <iostream>
#include <tuple>

class GameActor;
class Item;
class Terrain;
class Skybox;
class Font;

/**
 * @brief manages rendering of all game objects
 * 
 */
class RenderEngine : public Engine
{
private:
    GLFWwindow *window;
    std::vector<GameActor *> renderActorList;
    std::vector<Item *> renderItemList;
    std::vector<GameActor *> scoreboard;
    std::vector<Skybox *> fakesky;
    int scoreboardLength;
    Font *text;
    DefaultShader *textShader;
    DefaultShader *textUIShader;
    Terrain *terrain;

    /**
     * @brief method to call when the game is lost
     * 
     */
    void looseScreen();

    /**
     * @brief method to call when game is won
     * 
     */
    void winScreen();

public:
    /**
     * @brief Construct a new Render Engine object
     * 
     * @param mapTerrain game terrain
     * @param skybox skybox of the game
     * @param windowHeight height of the game window
     */
    RenderEngine(Terrain *mapTerrain, int windowHeight);

    /**
     * @brief Destroy the Render Engine object
     * 
     */
    ~RenderEngine();

    /**
     * @brief render all objects
     * 
     * @param timeDelta time since last frame
     */
    void update(float timeDelta) override;

    /**
     * @brief add a game object to the RenderEngine
     * 
     * @param obj object
     */
    void addGameObject(GameObject *obj);

    /**
     * @brief remove a game object from the RenderEngine
     * 
     * @param obj object
     */
    void removeGameObject(GameObject *obj);

    /**
     * @brief initialize scoreboard
     * 
     * @param length amount of scoreboard entries
     */
    void initScoreboard(int length);

    /**
     * @brief update the scoreboard
     * 
     */
    void updateScoreboard();
};

#endif