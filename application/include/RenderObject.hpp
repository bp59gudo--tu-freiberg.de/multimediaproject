#ifndef RENDER_OBJECT_HPP
#define RENDER_OBJECT_HPP

#include "ShaderProgram.hpp"

class Camera;

/**
 * @brief base class for all renderable objects
 * 
 */
class RenderObject
{
protected:
	ShaderProgram *program;
	static Camera *camReference;

	/**
	 * @brief Construct a new Render Object object
	 * 
	 * @param _shader shader
	 */
	RenderObject(ShaderProgram *_shader) : program(_shader)
	{
		DEBUG_METHOD("calling RenderObject::RenderObject");
	}

	/**
	 * @brief Destroy the Render Object object
	 * 
	 */
	~RenderObject()
	{
		DEBUG_METHOD("calling RenderObject::~RenderObject");
	}

public:
	/**
	 * @brief render the object
	 * 
	 */
	virtual void render() = 0;

	/**
	 * @brief set the main camera for all render objects 
	 * 
	 * @param gameCamera main camera
	 */
	static void initialize(Camera *gameCamera);
};

#endif