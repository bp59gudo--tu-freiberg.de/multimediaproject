#ifndef GAME_OBJECT_HPP
#define GAME_OBJECT_HPP

#include "RenderObject.hpp"
#include "PhysicsObject.hpp"
#include "Model3D.hpp"
#include "glm/vec3.hpp"
#include "glm/mat4x4.hpp"
#include "glm/gtx/transform.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

class Camera;
class Terrain;

/**
 * @brief base class for all physical and optical game objects in the game
 * 
 */
class GameObject : public RenderObject, public PhysicsObject
{
protected:
    Model3D *model;
    glm::vec3 position;
    glm::vec3 rotation;
    float scale;
    glm::mat4 modelMatrix;
    static Terrain *terrainReference;
    bool skipCollisionCheck;

    /**
     * @brief Construct a new Game Object object
     * 
     * @param _model model of the game object
     * @param shader shader of the game object
     * @param _position position of the game object
     * @param _scale scale of the game object
     */
    GameObject(Model3D *_model, ShaderProgram *shader, glm::vec3 _position = glm::vec3(0.0f, 0.0f, 0.0f), float _scale = 1.0f) : RenderObject(shader), model(_model), position(_position), rotation(glm::vec3(0.0f, 0.0f, 0.0f)), scale(_scale), PhysicsObject(_scale, glm::vec2(_position.x, _position.z))
    {
        DEBUG_METHOD("calling GameObject::GameObject");

        // initialize modelMatrix to identity matrix
        modelMatrix = glm::mat4(1.0f);

        // set model to its world position and scale it
        translateModel(position);
        scaleModel(scale);

        skipCollisionCheck = false;
    }

    /**
     * @brief Destroy the Game Object object
     * 
     */
    ~GameObject()
    {
        DEBUG_METHOD("calling GameObject::~GameObject");
    }

    /**
     * @brief translate the model by translation vector
     * 
     * @param _trans translation vector
     */
    void translateModel(glm::vec3 &_trans);

    /**
     * @brief rotate model by rotation angle
     * 
     * @param _angle_rad rotation angle (in RAD)
     */
    void rotateModel(float _angle_rad);

    /**
     * @brief scale model by scale factor
     * 
     * @param _scale scale factor
     */
    void scaleModel(float _scale);

public:
    /**
     * @brief enumerator of derived objects
     * 
     */
    typedef enum
    {
        GameActorType,
        ItemType,
        SkyboxType
    } ObjectType;

    /**
     * @brief skip the collision check for one frame (if collision check was already done by another game actor)
     * 
     */
    void skipCollision();

    /**
     * @brief Get the Object Type object -> to be implemented in all derived classes
     * 
     * @return ObjectType 
     */
    virtual ObjectType getObjectType() = 0;

    /**
     * @brief render the GameObject
     * 
     */
    virtual void render() = 0;

    /**
     * @brief called when two game objects are colliding -> to be implemented by derived classes
     * 
     * @param other other physics object
     */
    virtual void collide(PhysicsObject *other) = 0;

    /**
     * @brief initialize all GameObjects with the terrain
     * 
     * @param terrain game terrain
     */
    static void initialize(Terrain *terrain);

    /**
     * @brief Get the world position of the game object
     * 
     * @return glm::vec3 
     */
    glm::vec3 getPosition();

    /**
     * @brief Get the Scale of the object
     * 
     * @return float 
     */
    float getScale();
};

#endif