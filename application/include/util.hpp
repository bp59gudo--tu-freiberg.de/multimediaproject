#ifndef UTIL_HPP
#define UTIL_HPP

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include "glm/mat4x4.hpp"

#define debugging_enabled false
// ONLY SET THIS IF YOU EXPECT A CRASH
#define method_debugging_enabled false

/**
 * @brief print an error message
 * 
 */
#define errorMsg(msg)                           \
    std::cout << "ERROR: " << msg << std::endl; \
    exit(EXIT_FAILURE);

/**
 * @brief print error message when debugging is enabled -> based on https://stackoverflow.com/questions/14251038/debug-macros-in-c
 * 
 */
#define DEBUG(msg)         \
    if (debugging_enabled) \
    std::cerr << msg << std::endl

/**
 * @brief print method name each time a method is called (when method_debugging_enabled is set)
 * 
 */
#define DEBUG_METHOD(msg)         \
    if (method_debugging_enabled) \
    DEBUG(msg)

/**
 * @brief splits a string at a char -> based on https://stackoverflow.com/questions/5888022/split-string-by-single-spaces
 * 
 * @param str input string
 * @param content resulting substrings
 * @param delimiter char where to split
 */
inline void stringSplit(const std::string &str, std::vector<std::string> &content, char delimiter)
{
    size_t pos = str.find(delimiter);
    size_t initPos = 0;
    content.clear();

    while (pos != std::string::npos)
    {
        content.push_back(str.substr(initPos, pos - initPos));
        initPos = pos + 1;
        pos = str.find(delimiter, initPos);
    }
    content.push_back(str.substr(initPos, std::min(pos, str.size()) - initPos + 1));
}

/**
 * @brief checks if an error occured in OpenGL 
 * 
 * @param errorText message to write if an error occured
 */
inline void glCheckError(std::string errorText)
{
    GLenum error = glGetError();

    if (error != GL_NO_ERROR)
    {
        std::cerr << errorText << std::endl;
        exit(EXIT_FAILURE);
    }
}

/**
 * @brief checks if a condition is not true and prints an error message if that is the case
 * 
 * @param condition condition to test
 * @param errorText message to write if condition is false
 */
inline void checkError(int condition, std::string errorText)
{
    if (condition)
    {
        std::cerr << errorText << std::endl;
        exit(EXIT_FAILURE);
    }
}

/**
 * @brief prints a vec4
 * 
 * @param vec 
 */
inline void printVec4(glm::vec4 &vec)
{
    for (int i = 0; i < 4; i++)
    {
        std::cout << vec[i] << "\t ";
    }
    std::cout << std::endl;
}

/**
 * @brief prints a vec3
 * 
 * @param vec 
 */
inline void printVec3(glm::vec3 &vec)
{
    for (int i = 0; i < 3; i++)
    {
        std::cout << vec[i] << "\t ";
    }
    std::cout << std::endl;
}

/**
 * @brief prints a vec2
 * 
 * @param vec 
 */
inline void printVec2(glm::vec2 &vec)
{
    for (int i = 0; i < 2; i++)
    {
        std::cout << vec[i] << "\t ";
    }
    std::cout << std::endl;
}

/**
 * @brief prints a mat4
 * 
 * @param mat 
 */
inline void printMat4(glm::mat4 &mat)
{
    for (int c = 0; c < 4; c++)
    {
        printVec4(glm::transpose(mat)[c]);
    }
    std::cout << std::endl;
}

/**
 * @brief read file and return the lines in a vector. Allocates memory!
 * 
 * @param filename name of the file
 * @return std::vector<std::string>* 
 */
inline std::vector<std::string> *getFileContent(std::string filename)
{
    std::ifstream iFile(filename);

    if (!iFile.is_open())
    {
        DEBUG("could not open file " << filename);
        return nullptr;
    }

    std::string line;
    std::vector<std::string> *outVec = new std::vector<std::string>();
    while (std::getline(iFile, line))
    {
        if (line[0] != '#')
        {
            outVec->emplace_back(line);
        }
    }
    return outVec;
}

/**
 * @brief remove an item from a std::vector
 * 
 * @tparam t type of the item
 * @param vector vector holding the item
 * @param elem item
 */
template <typename t>
inline void removeFromVector(std::vector<t> &vector, t elem)
{
    auto iterator = vector.begin();
    while (iterator != vector.end())
    {
        if (elem == *iterator)
        {
            vector.erase(iterator);
            break;
        }
        iterator++;
    }
}

/**
 * @brief splits a string at a delimiter
 * 
 * @param str string
 * @param delimiter delimiter 
 * @param ignore chars that are ignored
 * @return std::vector<std::string> 
 */
inline std::vector<std::string> splitString(std::string str, char delimiter, char ignore)
{
    std::vector<std::string> outVec;
    outVec.clear();

    auto strIterator = str.begin();
    //remove whitespaces before line
    while (*strIterator == ' ' && *strIterator == '\t' && *strIterator == ignore)
    {
        strIterator++;
        //return empty if there are no elements except whitespace
        if (strIterator == str.end())
        {
            return outVec;
        }
    }

    while (strIterator != str.end())
    {
        std::string nextElement = "";

        while (*strIterator != delimiter && *strIterator != ignore && strIterator != str.end())
        {
            nextElement += *strIterator;
            strIterator++;
        }
        if (nextElement != "")
        {
            outVec.emplace_back(nextElement);
        }
        else
        {
            strIterator++;
        }
        
    }

    return outVec;
}

#endif