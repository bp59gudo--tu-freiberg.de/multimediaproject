#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "util.hpp"

class Engine
{
private:

protected:
    Engine() {}
    ~Engine() {}

public:
    virtual void update(float timeDelta) = 0;
};

#endif