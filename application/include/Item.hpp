#ifndef ITEM_HPP
#define ITEM_HPP

#include "GameObject.hpp"
#include "Camera.hpp"
#include "glm/vec3.hpp"

/**
 * @brief class for all game items
 * 
 */
class Item : public GameObject
{
private:
    // Texture sampler:
    GLint texLoc;
    GLint modelLoc, viewLoc, projectionLoc;

public:
    /**
     * @brief Construct a new Item object
     * 
     * @param _model model of the item
     * @param _shader shader of the item 
     * @param _position position of the item 
     * @param _scale scale of the item 
     */
    Item(Model3D *_model, ShaderProgram *_shader, glm::vec3 _position = glm::vec3(0.0f, 0.0f, 0.0f),
         float _scale = 1.0f) : GameObject(_model, _shader, _position, _scale)
    {
        DEBUG_METHOD("calling Item::Item");

        // get uniform locations
        texLoc = program->getUniformLoc("textureSampler");
        checkError(texLoc < 0, "Failed to obtain uniform location for textureSampler");
        modelLoc = program->getUniformLoc("uniform_model");
        checkError(modelLoc < 0, "Failed to obtain uniform location for uniform_model");
        viewLoc = program->getUniformLoc("uniform_view");
        checkError(viewLoc < 0, "Failed to obtain uniform location for uniform_view");
        projectionLoc = program->getUniformLoc("uniform_projection");
        checkError(projectionLoc < 0, "Failed to obtain uniform location for uniform_projection");

        collisionRadius = scale;
    }

    /**
     * @brief Destroy the Item object
     * 
     */
    ~Item()
    {
        DEBUG_METHOD("calling Item::~Item");
    }

    /**
     * @brief render the item (called by RenderEngine)
     * 
     */
    void render();

    /**
     * @brief Get the Object Type -> Item
     * 
     * @return ObjectType 
     */
    ObjectType getObjectType() override;

    /**
     * @brief called when colliding with an game actor or other item (empty, since all collision checks are done by GameActor)
     * 
     * @param other 
     */
    void collide(PhysicsObject *other) override;

    /**
     * @brief Get the Size 
     * 
     * @return float 
     */
    float getSize();

    /**
     * @brief Get a reference to the shader
     * 
     * @return ShaderProgram* 
     */
    ShaderProgram *getShaderProgram();

    /**
     * @brief rotate angle by rotation angle
     * 
     * @param angle rotation angle
     */
    void rotate(float angle);

    /**
     * @brief translate item by delta vector
     * 
     * @param translation delta translation vector
     */
    void translate(glm::vec3 translation);
};

#endif