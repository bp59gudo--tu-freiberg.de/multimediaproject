#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "GameUserData.hpp"
#include "GameState.hpp"
#include "glm/vec3.hpp"
#include "glm/mat4x4.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "util.hpp"

#include <functional>


void mouseCallback(GLFWwindow* window, double xpos, double ypos);
int main();

class Player;

/**
 * @brief Camera class, based on https://learnopengl.com/Getting-started/Camera
 * 
 */
class Camera
{
private:
    ///@param pos camera position
    glm::vec3 pos;
    ///@param front front direction of the camera
    glm::vec3 front;
    ///@param up up vector
    glm::vec3 up;
    ///@param yaw yaw of the camera
    float yaw;
    ///@param pitch pitch of the camera
    float pitch;
    ///@param viewMatrix camera view matrix
    glm::mat4 viewMatrix;
    ///@param projectionMatrix camera projection matrix
    glm::mat4 projectionMatrix;
    ///@param fov field of view
    float fov;
    ///@param nearPlane near plane distance
    float nearPlane;
    ///@param farPlane far plane distance
    float farPlane;

    ///@param firstMouse whether there has been mouse input before (only needed when camera free flight)
    bool firstMouse;
    /**
     * @param cameraLastX last X value from the mouse on the screen (only needed when camera free flight)
     * @param cameraLastY last Y value from the mouse on the screen (only needed when camera free flight)
     */
    float cameraLastX, cameraLastY;

    ///@brief friend methods to access private members
    friend void mouseCallback(GLFWwindow* window, double xpos, double ypos);
    friend int main();

    /**
     * @brief move the camera by a vec3
     * 
     * @param transform delta movement for the camera (view matrix)
     */
    void moveCamera(glm::vec3& transform);

    /**
     * @brief rotate the camera on the y axis
     * 
     * @param angle rotation angle (in rad)
     */
    void rotateCamera(float angle);


protected:
    ///@brief give GameState and Player access to protected members
    friend class GameState;
    friend class Player;

    /**
     * @brief Set the Window size
     * 
     */
    void setWindowSize();

    /**
     * @brief Set position and direction of the camera
     * 
     * @param position position vector
     * @param direction direction vector 
     */
    void setPosDir(glm::vec3 position, glm::vec3 direction);

public:
    /**
     * @brief Construct a new Camera object
     * 
     * @param windowWidth width of the window
     * @param windowHeight height of the window
     * @param position of the camera
     * @param target target to initially look at 
     * @param _fov (optional) field of view
     * @param _nearPlane (optional) distance of near plane
     * @param _farPlane (optional) distance of far plane
     */
    Camera(int windowWidth, int windowHeight, glm::vec3 position = glm::vec3(0.0f, 0.0f, 6.0f), glm::vec3 target = glm::vec3(0.0f, 0.0f, 0.0f),
        float _fov = 45.0f, float _nearPlane = 0.1f, float _farPlane = 800.0f);
    ~Camera();

    /**
     * @brief Get the View Matrix object
     * 
     * @return glm::mat4 
     */
    glm::mat4 getViewMatrix();

    /**
     * @brief Get the Projection Matrix object
     * 
     * @return glm::mat4 
     */
    glm::mat4 getProjectionMatrix();

    /**
     * @brief update the camera
     * 
     * @param deltaTime time since last update call
     * @param window game window pointer
     */
    void update(float deltaTime, GLFWwindow* window);

    /**
     * @brief Get the Position
     * 
     * @return glm::vec3 
     */
    glm::vec3 getPosition();

    /**
     * @brief Get the View direction
     * 
     * @return glm::vec3 
     */
    glm::vec3 getViewDir();
};

#endif