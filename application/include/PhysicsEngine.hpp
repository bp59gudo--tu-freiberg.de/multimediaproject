#ifndef PHYSICS_ENGINE_HPP
#define PHYSICS_ENGINE_HPP

#include <vector>

#include "Engine.hpp"
#include "PhysicsObject.hpp"

/**
 * @brief engine managing all physics objects in a scene
 * 
 */
class PhysicsEngine : public Engine
{
private:
    std::vector<PhysicsObject*> physicsObjects;
public:
    /**
     * @brief Construct a new Physics Engine object
     * 
     */
    PhysicsEngine();

    /**
     * @brief Destroy the Physics Engine object
     * 
     */
    ~PhysicsEngine();
    
    /**
     * @brief update the physics of all added PhysicsObjects by a time delta
     * 
     * @param timeDelta amount of passed time
     */
    void update(float timeDelta);

    /**
     * @brief add a derivative of PhysicsObject to the engine
     * 
     * @param obj derivative of PhysicsObject
     */
    void addPhysicsObject(PhysicsObject* obj);

    /**
     * @brief remove a derivative of PhysicsObject from the engine
     * 
     * @param obj pointer to the object
     */
    void removePhysicsObject(PhysicsObject* obj);

};

#endif