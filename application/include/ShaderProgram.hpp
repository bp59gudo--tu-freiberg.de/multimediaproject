#ifndef SHADER_PROGRAM_HPP
#define SHADER_PROGRAM_HPP

#include "util.hpp"
#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

/**
 * @brief base class for all shaders. Based on lecture and https://www.youtube.com/watch?v=oc8Yl4ZruCA&list=PLRIWtICgwaX0u7Rf9zkZhLoLuZVfUksDP&index=7
 * 
 */
class ShaderProgram
{
protected:
	GLuint programPtr;

	/**
	 * @brief Construct a new Shader Program object
	 * 
	 */
	ShaderProgram()
	{
		DEBUG_METHOD("calling ShaderProgram::ShaderProgram");
	}

	/**
	 * @brief Destroy the Shader Program object
	 * 
	 */
	~ShaderProgram();

	/**
     * @brief read a shader file from disk
     * 
     * @param path path to file
     * @return content as string
     */
	std::string readShaderFile(std::string path);

	/**
     * @brief compiles a shader file from disk
     * 
     * @param type type of the shader
     * @param path path to the file
     * @param tag additional tag for the shader
     * @return GLuint pointer to the shader
     */
	GLuint compileShader(GLenum type, std::string path, std::string tag);

	/**
	 * @brief bind shader attribute
	 * 
	 * @param attributePosition position of the attribute
	 * @param variableName name of the attribute
	 */
	void bindAttribute(int attributePosition, std::string variableName);

	/**
	 * @brief initialize ShaderProgram
	 * 
	 */
	virtual void initProgram() = 0;

public:
	/**
	 * @brief Get the program pointer
	 * 
	 * @return GLuint 
	 */
	GLuint getProgramPtr();

	/**
	 * @brief Get the Uniform location to an uniform
	 * 
	 * @param name uniform name
	 * @return GLint 
	 */
	GLint getUniformLoc(std::string name);
};

#endif