#include "../include/Model3D.hpp"

Model3D::Model3D(std::vector<float> *positions, Model3D::Material& mat, std::vector<int> *indices, std::vector<unsigned char> *colors,
                 std::vector<float> *normals, std::vector<float> *texCoords, std::string texturePath)
{
	DEBUG_METHOD("calling Model3D::Model3D(std::vector<float> *positions, Model3D::Material& mat, std::vector<int> *indices, std::vector<unsigned char> *colors,\
                 std::vector<float> *normals, std::vector<float> *texCoords, std::string texturePath)");
    initialize(positions, mat, indices, colors, normals, texCoords, texturePath);
}

void Model3D::initialize(std::vector<float> *positions, Material& mat, std::vector<int> *indices, std::vector<unsigned char> *colors,
                         std::vector<float> *normals, std::vector<float> *texCoords, std::string texturePath)
{
	DEBUG_METHOD("calling Model3D::initialize");
    //convert vector to array (elements are appearently stored contiguously) -> https://stackoverflow.com/questions/2923272/how-to-convert-vector-to-array
    float *posArray = &(*positions)[0];
    unsigned long posSize = positions->size() * sizeof(float);

    vertexDataCount = positions->size();

    checkError((positions->size() % 3), "positions not multiple of three!");

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    hasColors = hasTextureCoords = hasNormals = false;

    storeDataInAttribList(ATTRIB_POSITION, posArray, GL_FLOAT, 3, posSize, GL_FALSE);

    if(indices != nullptr)
    {
        //convert vector to array (elements are appearently stored contiguously)
        int* indicesPtr = &(*indices)[0];
        bindIndicesBuffer(indicesPtr, indices->size() * sizeof(int));
    }

    if (colors != nullptr)
    {
        //convert vector to array (elements are appearently stored contiguously)
        unsigned char *colArray = &(*colors)[0];
        unsigned long colSize = colors->size() * sizeof(unsigned char);
        storeDataInAttribList(ATTRIB_COLOR, colArray, GL_UNSIGNED_BYTE, 3, colSize, GL_TRUE);
        hasColors = true;
    }

    if (normals != nullptr)
    {
        //convert vector to array (elements are appearently stored contiguously)
        float *normalArray = &(*normals)[0];
        unsigned long normalSize = normals->size() * sizeof(float);
        storeDataInAttribList(ATTRIB_NORMAL, normalArray, GL_FLOAT, 3, normalSize, GL_FALSE);
        hasNormals = true;
    }

    if (texCoords != nullptr && texturePath != "")
    {
        //convert vector to array (elements are appearently stored contiguously)
        float *tcArray = &(*texCoords)[0];
        unsigned long tcSize = colors->size() * sizeof(float);
        storeDataInAttribList(ATTRIB_TEX_COORDS, tcArray, GL_FLOAT, 2, tcSize, GL_FALSE);

        glGenTextures(1, &texturePtr);
        glCheckError("glGenTextures");

        glBindTexture(GL_TEXTURE_2D, texturePtr);
        glCheckError("glBindTexture");

        //set texture parameters
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glCheckError("glTexParameteri [wrap s]");
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glCheckError("glTexParameteri [wrap t]");
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glCheckError("glTexParameteri [min filter]");
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glCheckError("glTexParameteri [mag filter]");

        //load pixels from bmp
        bitmap_pixel_rgb_t *pixels;
        int imageWidth, imageHeight;
        bitmap_error_t error = bitmapReadPixels(texturePath.c_str(), (bitmap_pixel_t **)&pixels, &imageWidth, &imageHeight, BITMAP_COLOR_SPACE_RGB);
        checkError(error != BITMAP_ERROR_SUCCESS, "Failed to load texture");
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
        glCheckError("glTexImage2D");

        free(pixels);

        hasTextureCoords = true;
    }
    vaoPtr = vao;
}

Model3D::Model3D(std::string objFilePath, std::string texturePath, Material& mat)
{
	DEBUG_METHOD("calling Model3D::Model3D(std::string objFilePath, std::string texturePath, Material& mat)");
    //based on http://www.opengl-tutorial.org/beginners-tutorials/tutorial-7-model-loading/
    std::vector<int> vertexIndices, uvIndices, normalIndices;

    std::vector<glm::vec3> tempVertices;
    std::vector<glm::vec2> tempUVs;
    std::vector<glm::vec3> tempNormals;

    //load .obj file
    std::ifstream objFile(objFilePath);
    if (!objFile)
    {
        //try with relative path
        objFile = std::ifstream("../data/models/" + objFilePath);
        if (!objFile)
        {
            std::cerr << "Couldn't find file " << objFilePath << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    //check if texture exists
    if(!std::experimental::filesystem::exists(texturePath))
    {
        std::string texturePathOrig = texturePath;
        texturePath = "../data/textures/" + texturePath;
        if(!std::experimental::filesystem::exists(texturePath))
        {
            std::cerr << "Couldn't find file " << texturePathOrig << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    //read .obj file format (same as in lecture, just in C++)
    std::string line;
    std::vector<std::string> lineContent;
    glm::vec3 currentVertex, currentNormal;
    glm::vec2 currentUV;
    while (std::getline(objFile, line))
    {
        stringSplit(line, lineContent, ' ');

        if (lineContent[0] == "v")
        {
            currentVertex = glm::vec3(std::stof(lineContent[1]), std::stof(lineContent[2]), std::stof(lineContent[3]));
            tempVertices.emplace_back(currentVertex);
        }
        else if (lineContent[0] == "vt")
        {
            currentUV = glm::vec2(std::stof(lineContent[1]), std::stof(lineContent[2]));
            tempUVs.emplace_back(currentUV);
        }
        else if (lineContent[0] == "vn")
        {
            currentNormal = glm::vec3(std::stof(lineContent[1]), std::stof(lineContent[2]), std::stof(lineContent[3]));
            tempNormals.emplace_back(currentNormal);
        }
        else if (lineContent[0] == "f")
        {
            //vectors storing the vertex content
            std::vector<std::string> vertex1, vertex2, vertex3;
            unsigned vertexIndex[3], uvIndex[3], normalIndex[3];

            stringSplit(lineContent[1], vertex1, '/');
            stringSplit(lineContent[2], vertex2, '/');
            stringSplit(lineContent[3], vertex3, '/');

            //fill indices
            vertexIndex[0] = std::stoi(vertex1[0]);
            uvIndex[0] = std::stoi(vertex1[1]);
            normalIndex[0] = std::stoi(vertex1[2]);

            vertexIndex[1] = std::stoi(vertex2[0]);
            uvIndex[1] = std::stoi(vertex2[1]);
            normalIndex[1] = std::stoi(vertex2[2]);

            vertexIndex[2] = std::stoi(vertex3[0]);
            uvIndex[2] = std::stoi(vertex3[1]);
            normalIndex[2] = std::stoi(vertex3[2]);

            //push indices
            vertexIndices.emplace_back(vertexIndex[0]);
            vertexIndices.emplace_back(vertexIndex[1]);
            vertexIndices.emplace_back(vertexIndex[2]);
            uvIndices.emplace_back(uvIndex[0]);
            uvIndices.emplace_back(uvIndex[1]);
            uvIndices.emplace_back(uvIndex[2]);
            normalIndices.emplace_back(normalIndex[0]);
            normalIndices.emplace_back(normalIndex[1]);
            normalIndices.emplace_back(normalIndex[2]);
        }
    }

    std::vector<float> vertices, uvs, normals;
    std::vector<unsigned char> colors;

    for (auto &vi : vertexIndices)
    {
        unsigned int vertexIndex = vi;

        glm::vec3 vertex = tempVertices[vertexIndex - 1];
        vertices.emplace_back(vertex.x);
        vertices.emplace_back(vertex.y);
        vertices.emplace_back(vertex.z);

        colors.emplace_back(0xFF);
        colors.emplace_back(0xFF);
        colors.emplace_back(0xFF);
    }

    for (auto &uvi : uvIndices)
    {
        unsigned int uvIndex = uvi;

        glm::vec2 uv = tempUVs[uvIndex - 1];
        uvs.emplace_back(uv.x);
        uvs.emplace_back(uv.y);
    }

    for (auto &ni : normalIndices)
    {
        unsigned int normalIndex = ni;

        glm::vec3 normal = tempNormals[normalIndex - 1];
        normals.emplace_back(normal.x);
        normals.emplace_back(normal.y);
        normals.emplace_back(normal.z);
    }

    initialize(&vertices, mat, &vertexIndices, &colors, &normals, &uvs, texturePath);

    hasNormals = hasColors = hasTextureCoords = true;
    DEBUG("created " << objFilePath);
}

Model3D::Model3D(std::string objFilePath, unsigned char colorR, unsigned char colorG, unsigned char colorB, Material& mat)
{
	DEBUG_METHOD("calling Model3D::Model3D(std::string objFilePath, unsigned char colorR, unsigned char colorG, unsigned char colorB, Material& mat)");
    //based on http://www.opengl-tutorial.org/beginners-tutorials/tutorial-7-model-loading/
    std::vector<int> vertexIndices, normalIndices;

    std::vector<glm::vec3> tempVertices;
    std::vector<glm::vec2> tempUVs;
    std::vector<glm::vec3> tempNormals;

    //load .obj file
    std::ifstream objFile(objFilePath);
    if (!objFile)
    {
        //try with relative path
        objFile = std::ifstream("../data/models/" + objFilePath);
        if (!objFile)
        {
            std::cerr << "Couldn't find file " << objFilePath << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    std::string line;
    std::vector<std::string> lineContent;
    glm::vec3 currentVertex, currentNormal;
    glm::vec2 currentUV;
    while (std::getline(objFile, line))
    {
        stringSplit(line, lineContent, ' ');

        if (lineContent[0] == "v")
        {
            currentVertex = glm::vec3(std::stof(lineContent[1]), std::stof(lineContent[2]), std::stof(lineContent[3]));
            tempVertices.emplace_back(currentVertex);
        }
        else if (lineContent[0] == "vt")
        {
            currentUV = glm::vec2(std::stof(lineContent[1]), std::stof(lineContent[2]));
            tempUVs.emplace_back(currentUV);
        }
        else if (lineContent[0] == "vn")
        {
            currentNormal = glm::vec3(std::stof(lineContent[1]), std::stof(lineContent[2]), std::stof(lineContent[3]));
            tempNormals.emplace_back(currentNormal);
        }
        else if (lineContent[0] == "f")
        {
            //vectors storing the vertex content
            std::vector<std::string> vertex1, vertex2, vertex3;
            unsigned vertexIndex[3], uvIndex[3], normalIndex[3];

            stringSplit(lineContent[1], vertex1, '/');
            stringSplit(lineContent[2], vertex2, '/');
            stringSplit(lineContent[3], vertex3, '/');

            //fill indices
            vertexIndex[0] = std::stoi(vertex1[0]);
            uvIndex[0] = std::stoi(vertex1[1]);
            normalIndex[0] = std::stoi(vertex1[2]);

            vertexIndex[1] = std::stoi(vertex2[0]);
            uvIndex[1] = std::stoi(vertex2[1]);
            normalIndex[1] = std::stoi(vertex2[2]);

            vertexIndex[2] = std::stoi(vertex3[0]);
            uvIndex[2] = std::stoi(vertex3[1]);
            normalIndex[2] = std::stoi(vertex3[2]);

            //push indices
            vertexIndices.emplace_back(vertexIndex[0]);
            vertexIndices.emplace_back(vertexIndex[1]);
            vertexIndices.emplace_back(vertexIndex[2]);
            normalIndices.emplace_back(normalIndex[0]);
            normalIndices.emplace_back(normalIndex[1]);
            normalIndices.emplace_back(normalIndex[2]);
        }
    }

    std::vector<float> vertices, normals;
    std::vector<unsigned char> colors;

    for (auto &vi : vertexIndices)
    {
        unsigned int vertexIndex = vi;

        glm::vec3 vertex = tempVertices[vertexIndex - 1];
        vertices.emplace_back(vertex.x);
        vertices.emplace_back(vertex.y);
        vertices.emplace_back(vertex.z);

        colors.emplace_back(colorR);
        colors.emplace_back(colorG);
        colors.emplace_back(colorB);
    }

    for (auto &ni : normalIndices)
    {
        unsigned int normalIndex = ni;

        glm::vec3 normal = tempNormals[normalIndex - 1];
        normals.emplace_back(normal.x);
        normals.emplace_back(normal.y);
        normals.emplace_back(normal.z);
    }

    initialize(&vertices, mat, &vertexIndices, &colors, &normals);

    hasNormals = hasColors = true;
    hasTextureCoords = false;
    DEBUG("created " << objFilePath);
}

Model3D::~Model3D()
{
	DEBUG_METHOD("calling Model3D::~Model3D");
    glDeleteVertexArrays(1, &vaoPtr);
    glCheckError("glDeleteVertexArrays");
    for (auto &vboTuple : vboPtrList)
    {
        glDeleteBuffers(1, &std::get<1>(vboTuple));
        glCheckError("glDeleteBuffers");
    }
    if (hasTextureCoords)
    {
        glDeleteTextures(1, &texturePtr);
    }
}

GLuint Model3D::getVAO() const
{
	DEBUG_METHOD("calling Model3D::getVAO");
    return vaoPtr;
}

GLuint Model3D::getTextureID() const
{
	DEBUG_METHOD("calling Model3D::getTextureID");
    return texturePtr;
}

void Model3D::bindVAO()
{
	DEBUG_METHOD("calling Model3D::bindVAO");
    glBindVertexArray(vaoPtr);
    glCheckError("glBindVertexArray -> bind vao");
}

void Model3D::unbindVAO()
{
	DEBUG_METHOD("calling Model3D::unbindVAO");
    glBindVertexArray(0);
    glCheckError("glBindVertexArray -> unbind vao");
}

unsigned Model3D::getVertexCount()
{
    DEBUG_METHOD("calling Model3D::getVertexCount");
    return vertexDataCount;
}

void Model3D::objCountEntries(std::ifstream &objFile, int &vertexCount, int &texCoordsCount, int &normalCount, int &faceCount, int &mtlLibCount)
{
	DEBUG_METHOD("calling Model3D::objCountEntries");
    if (!objFile)
    {
        std::cerr << "Couldn't find file" << std::endl;
        exit(EXIT_FAILURE);
    }

    //reset counters
    vertexCount = texCoordsCount = normalCount = faceCount = mtlLibCount = 0;

    std::string line;
    std::vector<std::string> lineContent;
    while (std::getline(objFile, line))
    {
        stringSplit(line, lineContent, ' ');

        if (lineContent[0] == "v")
            vertexCount++;
        else if (lineContent[0] == "vt")
            texCoordsCount++;
        else if (lineContent[0] == "vn")
            normalCount++;
        else if (lineContent[0] == "f")
            faceCount++;
        else if (lineContent[0] == "mtllib")
            mtlLibCount++;
    }
}

Model3D::ObjEntryType_t Model3D::objGetNextEntry(std::ifstream &objFile, Model3D::ObjEntry_t *entry)
{
	DEBUG_METHOD("calling Model3D::objGetNextEntry");

    // Track the indices of the next elements we receive:
    int nextVertexIndex = 0;
    int nextTexCoordsIndex = 0;
    int nextNormalIndex = 0;

    // Read line by line:
    std::string line;
    std::vector<std::string> lineContent;
    while (std::getline(objFile, line))
    {
        // Skip potential spaces:
        stringSplit(line, lineContent, ' ');

        if (lineContent[0] == "v")
        {
            // Parse the vertex:
            entry->vertexEntry.x = std::stof(lineContent[1]);
            entry->vertexEntry.y = std::stof(lineContent[2]);
            entry->vertexEntry.z = std::stof(lineContent[3]);
            entry->vertexEntry.w = -1;

            nextVertexIndex++;

            return OBJ_ENTRY_TYPE_VERTEX;
        }
        else if (lineContent[0] == "vt")
        {
            //curr_line += 2;

            // Parse the vertex:
            entry->texCoordsEntry.u = std::stof(lineContent[1]);
            entry->texCoordsEntry.v = std::stof(lineContent[2]);

            nextTexCoordsIndex++;

            return OBJ_ENTRY_TYPE_TEX_COORDS;
        }
        else if (lineContent[0] == "vn")
        {
            //curr_line += 2;

            // Parse the vertex:
            entry->normalEntry.x = std::stof(lineContent[1]);
            entry->normalEntry.y = std::stof(lineContent[2]);
            entry->normalEntry.z = std::stof(lineContent[3]);

            nextNormalIndex++;

            return OBJ_ENTRY_TYPE_NORMAL;
        }
        else if (lineContent[0] == "f")
        {
            //curr_line++;

            for (int i = 0; i < 3; i++)
            {
                std::vector<std::string> currentVertex;
                stringSplit(lineContent[i + 1], currentVertex, '/');

                entry->faceEntry.triples[i].texCoordsIndex = -1;
                entry->faceEntry.triples[i].normalIndex = -1;

                // Vertex index:
                entry->faceEntry.triples[i].vertexIndex = std::stol(currentVertex[0]) - 1;

                if (entry->faceEntry.triples[i].vertexIndex < 0)
                {
                    entry->faceEntry.triples[i].vertexIndex += nextVertexIndex + 1;
                }

                // Texture coordinates index:
                entry->faceEntry.triples[i].texCoordsIndex = std::stol(currentVertex[1]) - 1;

                if (entry->faceEntry.triples[i].texCoordsIndex < 0)
                {
                    entry->faceEntry.triples[i].texCoordsIndex += nextTexCoordsIndex + 1;
                }

                // Normal index:
                entry->faceEntry.triples[i].normalIndex = std::stol(currentVertex[2]) - 1;

                if (entry->faceEntry.triples[i].normalIndex < 0)
                {
                    entry->faceEntry.triples[i].normalIndex += nextNormalIndex + 1;
                }
            }

            return OBJ_ENTRY_TYPE_FACE;
        }
    }

    // No line left.
    return OBJ_ENTRY_TYPE_END;
}

void Model3D::bindIndicesBuffer(int *indices, GLsizeiptr size)
{
	DEBUG_METHOD("calling Model3D::bindIndicesBuffer");
    GLuint indicesVBO;
    glGenBuffers(1, &indicesVBO);
    glCheckError("glGenBuffers -> index buffer");
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVBO);
    glCheckError("glBindBuffer -> index buffer");
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, indices, GL_STATIC_DRAW);
    glCheckError("glBufferData -> index buffer");
    vboPtrList.emplace_back(std::tuple<int, GLuint>(ATTRIB_INDEX, indicesVBO));
}

template <typename t>
void Model3D::storeDataInAttribList(int attribNumber, t *data, GLenum dataType, GLint numberOfComponents, GLint size, GLboolean normalized)
{
	DEBUG_METHOD("calling Model3D::storeDataInAttribList");
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glCheckError("glGenBuffers -> attrib buffer, attrib pos " + std::to_string(attribNumber));
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glCheckError("glBindBuffer -> attrib buffer, attrib pos " + std::to_string(attribNumber));
    glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
    glCheckError("glBufferData -> attrib buffer, attrib pos " + std::to_string(attribNumber));
    glVertexAttribPointer(attribNumber, numberOfComponents, dataType, normalized, 0, 0);
    glCheckError("glVertexAttribPointer -> attrib buffer, attrib pos " + std::to_string(attribNumber));
    glEnableVertexAttribArray(attribNumber);
    glCheckError("glEnableVertexAttribArray " + std::to_string(attribNumber));
    //add to vbo list
    vboPtrList.emplace_back(std::tuple<int, GLuint>(attribNumber, vbo));
}

bool Model3D::hasColor()
{
	DEBUG_METHOD("calling Model3D::hasColor");
    return hasColors;
}

bool Model3D::hasNormal()
{
	DEBUG_METHOD("calling Model3D::hasNormal");
    return hasNormals;
}

bool Model3D::hasTexture()
{
	DEBUG_METHOD("calling Model3D::hasTexture");
    return hasTextureCoords;
}

Model3D::Material Model3D::getMaterial()
{
	DEBUG_METHOD("calling Model3D::getMaterial");
    return modelMaterial;
}