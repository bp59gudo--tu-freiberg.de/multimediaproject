#include "../include/Terrain.hpp"

float Terrain::getX()
{
	DEBUG_METHOD("calling Terrain::getX");
    return xcoord;
}

float Terrain::getZ()
{
	DEBUG_METHOD("calling Terrain::getZ");
    return zcoord;
}

Model3D *Terrain::getModel3D() const
{
	DEBUG_METHOD("calling Terrain::getModel3D");
    return model;
}

float Terrain::getHeight(int x, int z)
{
	DEBUG_METHOD("calling Terrain::getHeight");
    return generator.generateHeight(x, z);
}

int Terrain::getSIZE()
{
	DEBUG_METHOD("calling Terrain::getSIZE");
    return SIZE;
}

float Terrain::getVERTEX_COUNT()
{
	DEBUG_METHOD("calling Terrain::getVERTEX_COUNT");
    return VERTEX_COUNT;
}

//https://de.wikipedia.org/wiki/Baryzentrische_Koordinaten
float Terrain::barycentricInterpolation(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos)
{
	DEBUG_METHOD("calling Terrain::barycentricInterpolation");
    float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
    float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
    float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
    float l3 = 1.0f - l1 - l2;

    return l1 * p1.y + l2 * p2.y + l3 * p3.y;
}

float Terrain::getHeightOfTerrain(float worldX, float worldZ, bool &passedTerrainBorder)
{
	DEBUG_METHOD("calling Terrain::getHeightOfTerrain");
    int heightsXLength = (int)sqrt(heights.size());

    //translate to terrain coordinates
    float terrainX = worldX - xcoord;
    float terrainZ = worldZ - zcoord;

    float gridSquareSize = SIZE / ((float)heightsXLength - 1);
    int gridX = (int)floor(terrainX / gridSquareSize);
    int gridZ = (int)floor(terrainZ / gridSquareSize);

    //going full over terrain
    if (gridX >= heightsXLength - 1 || gridZ >= heightsXLength - 1 || gridX < 0 || gridZ < 0)
    {
        passedTerrainBorder = true;
        return 0.0f;
    }

    float xCoord = fmod(terrainX, gridSquareSize) / gridSquareSize;
    float zCoord = fmod(terrainZ, gridSquareSize) / gridSquareSize;

    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            auto elem = heights[(gridX + i) * heightsXLength + gridZ + j];
        }
    }

    float result = 0.0f;
    //check which triangle of the square the player is standing on and interpolate the height between the triangle edges
    if (xCoord <= (1 - zCoord))
    {
        result = barycentricInterpolation(
            glm::vec3(0.0f, heights[gridX * heightsXLength + gridZ], 0.0f),
            glm::vec3(1.0f, heights[(gridX + 1) * heightsXLength + gridZ], 0.0f),
            glm::vec3(0.0f, heights[gridX * heightsXLength + gridZ + 1], 1.0f),
            glm::vec2(xCoord, zCoord));
    }
    else
    {
        result = barycentricInterpolation(
            glm::vec3(1.0f, heights[(gridX + 1) * heightsXLength + gridZ], 0.0f),
            glm::vec3(1.0f, heights[(gridX + 1) * heightsXLength + gridZ + 1], 1.0f),
            glm::vec3(0.0f, heights[gridX * heightsXLength + gridZ + 1], 1.0f),
            glm::vec2(xCoord, zCoord));
    }

    //going over the threshold of 3m
    if (gridX >= heightsXLength - 4 || gridZ >= heightsXLength - 4 || gridX < 4 || gridZ < 4)
    {
        passedTerrainBorder = true;
    }
    return result;
}

float Terrain::generateHeight(float x, float z)
{
	DEBUG_METHOD("calling Terrain::generateHeight");
    return generator.generateHeight(x, z);
}

glm::vec3 Terrain::calcNormal(int x, int z)
{
	DEBUG_METHOD("calling Terrain::calcNormal");
    float heightL = getHeight(x - 1, z);
    float heightR = getHeight(x + 1, z);
    float heightD = getHeight(x, z - 1);
    float heightU = getHeight(x, z + 1);
    glm::vec3 normal = glm::vec3(heightL - heightR, 2.0f, heightD - heightU);
    normal = glm::normalize(normal);
    return normal;
}

void Terrain::generateTerrain(int vertexCount, int size)
{
	DEBUG_METHOD("calling Terrain::generateTerrain");
    std::vector<float> vertices;
    std::vector<float> normals;
    std::vector<float> texCoords;
    std::vector<int> indices;
    heights.clear();
    heights.resize(vertexCount * vertexCount);
    for (int i = 0; i < vertexCount; i++)
    {
        for (int j = 0; j < vertexCount; j++)
        {
            vertices.emplace_back((float)j / ((float)vertexCount - 1) * size);

            //set higher edge to map so that balls cannot fall over the map
            float higherEdge = 0.0f;
    
                if (j == 1 || j == (vertexCount - 2) || i == 1 || i == (vertexCount - 2))
                {
                    higherEdge = GameSettings::instance()->getMountainEdge() * 0.9f;
                }
                else
                {
                    if (j == 2 || j == (vertexCount - 3) || i == 2 || i == (vertexCount - 3))
                    {
                        higherEdge = GameSettings::instance()->getMountainEdge();
                    }
                    else
                    {
                        if (j == 3 || j == (vertexCount - 4) || i == 3 || i == (vertexCount - 4))
                        {
                            higherEdge = GameSettings::instance()->getMountainEdge() / 2.0f;
                        }
                        else
                        {
                            if (j == 4 || j == (vertexCount - 5) || i == 4 || i == (vertexCount - 5))
                            {
                                higherEdge = GameSettings::instance()->getMountainEdge() / 6.0f;
                            }
                        }
                    }
                }
            

            float height = getHeight(j, i) + higherEdge;
            vertices.emplace_back(height);
            heights[j * vertexCount + i] = height;
            vertices.emplace_back((float)i / ((float)vertexCount - 1) * size);
            glm::vec3 normal = calcNormal(j, i); //glm::vec3(0.0, 1.0, 0.0);
            normals.emplace_back(normal.x);
            normals.emplace_back(normal.y);
            normals.emplace_back(normal.z);
            texCoords.emplace_back((float)j / ((float)vertexCount - 1));
            texCoords.emplace_back((float)i / ((float)vertexCount - 1));
        }
    }

    for (int gz = 0; gz < vertexCount - 1; gz++)
    {
        for (int gx = 0; gx < vertexCount - 1; gx++)
        {
            int topLeft = (gz * vertexCount) + gx;
            int topRight = topLeft + 1;
            int bottomLeft = ((gz + 1) * vertexCount) + gx;
            int bottomRight = bottomLeft + 1;
            indices.emplace_back(topLeft);
            indices.emplace_back(bottomLeft);
            indices.emplace_back(topRight);
            indices.emplace_back(topRight);
            indices.emplace_back(bottomLeft);
            indices.emplace_back(bottomRight);
        }
    }

    //generate colors based on the height
    std::vector<unsigned char> colors;
    float maxHeight = generator.getMaxAmplitude();
    float middleHeight = 0.5f * maxHeight;

    /** green landscape:
    float baseR = 0x4F;
    float baseG = 0x7A;
    float baseB = 0x18;

    float middleR = 0xB5;
    float middleG = 0x90;
    float middleB = 0x6C;

    float maxR = 0xE2;
    float maxG = 0xE2;
    float maxB = 0xE2;
    */

    float baseR = 255;
    float baseG = 249;
    float baseB = 227;

    float middleR = 215;
    float middleG = 190;
    float middleB = 169;

    float maxR = 203;
    float maxG = 178;
    float maxB = 161;

    for (int i = 0; i < vertices.size(); i += 3)
    {
        float height = vertices[i + 1];
        if (height <= middleHeight)
        {
            colors.emplace_back((unsigned char)generator.interpolate(baseR, middleR, height / middleHeight));
            colors.emplace_back((unsigned char)generator.interpolate(baseG, middleG, height / middleHeight));
            colors.emplace_back((unsigned char)generator.interpolate(baseB, middleB, height / middleHeight));
        }
        else if (height <= maxHeight)
        {
            colors.emplace_back((unsigned char)generator.interpolate(middleR, maxR, height / maxHeight));
            colors.emplace_back((unsigned char)generator.interpolate(middleG, maxG, height / maxHeight));
            colors.emplace_back((unsigned char)generator.interpolate(middleB, maxB, height / maxHeight));
        }
        else
        {
            colors.emplace_back((unsigned char)maxR);
            colors.emplace_back((unsigned char)maxG);
            colors.emplace_back((unsigned char)maxB);
        }
    }

    DEBUG("Terrain vertices:");
    DEBUG(vertices.size());
    DEBUG("Terrain indices:");
    DEBUG(indices.size());

    Model3D::Material mat;

    model = new Model3D(&vertices, mat, &indices, &colors, &normals);
}

void Terrain::render()
{
	DEBUG_METHOD("calling Terrain::render");
    glUseProgram(program->getProgramPtr());
    glCheckError("glUseProgram");

    glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(camReference->getProjectionMatrix()));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(camReference->getViewMatrix()));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelMatrix));
    glCheckError("glUniformMatrix4fv");

    model->bindVAO();

    glEnableVertexAttribArray(ATTRIB_POSITION);
    glEnableVertexAttribArray(ATTRIB_COLOR);
    glEnableVertexAttribArray(ATTRIB_NORMAL);
    glCheckError("glEnableVertexAttribArray");

    glDrawElements(GL_TRIANGLES, model->getVertexCount() * 3, GL_UNSIGNED_INT, NULL);
    glCheckError("glDrawElements");

    glDisableVertexAttribArray(ATTRIB_POSITION);
    glDisableVertexAttribArray(ATTRIB_COLOR);
    glDisableVertexAttribArray(ATTRIB_NORMAL);
    glCheckError("glDisableVertexAttribArray");

    model->unbindVAO();
}