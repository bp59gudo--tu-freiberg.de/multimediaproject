#include "../include/PhysicsObject.hpp"

unsigned PhysicsObject::physicsObjectsAmount = 0;

PhysicsObject::~PhysicsObject()
{
	DEBUG_METHOD("calling PhysicsObject::~PhysicsObject");
}

void PhysicsObject::updatePhysics(float timeDelta, const std::vector<PhysicsObject *> &objects)
{
	DEBUG_METHOD("calling PhysicsObject::updatePhysics");
    for (auto &elem : objects)
    {
        //if elem is not this object itself
        if (physicsID != elem->getPhysicsID())
        {
            //distance to element
            float distance = glm::distance(center2D, elem->center2D);

            //2D collision check -> when distance is smaller than own radius + others radius
            if (distance <= this->collisionRadius + elem->collisionRadius)
            {
                //check if elements did not collide in this frame already
                for (auto &collider : collidedWithinFrame)
                {
                    if (elem->getPhysicsID() == collider)
                        return;
                }

                //otherwise add element to collided within frame list
                collidedWithinFrame.emplace_back(elem->getPhysicsID());
                elem->collidedWithinFrame.emplace_back(physicsID);
                collide(elem);
            }
        }
    }
}

unsigned PhysicsObject::getPhysicsID()
{
	DEBUG_METHOD("calling PhysicsObject::getPhysicsID");
    return physicsID;
}

void PhysicsObject::resetCollision()
{
	DEBUG_METHOD("calling PhysicsObject::resetCollision");
    collidedWithinFrame.clear();
}

bool operator==(const PhysicsObject &first, const PhysicsObject &second)
{
	DEBUG_METHOD("calling operator==(const PhysicsObject &first, const PhysicsObject &second)");
    return first.physicsID == second.physicsID;
}

bool operator!=(const PhysicsObject &first, const PhysicsObject &second)
{
	DEBUG_METHOD("calling operator!=(const PhysicsObject &first, const PhysicsObject &second)");
    return first.physicsID != second.physicsID;
}