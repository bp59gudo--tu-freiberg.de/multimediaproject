#include "../include/ShaderProgram.hpp"

ShaderProgram::~ShaderProgram()
{
	DEBUG_METHOD("calling ShaderProgram::~ShaderProgram");
	glDeleteProgram(programPtr);
	glCheckError("glDeleteProgram");
}

std::string ShaderProgram::readShaderFile(std::string path)
{
	DEBUG_METHOD("calling ShaderProgram::readShaderFile");
    std::ifstream shaderCode(path);
	if (!shaderCode)
    {
        //try with relative path
        shaderCode = std::ifstream("../shader/" + path);
        if (!shaderCode)
        {
            std::cerr << "Couldn't find shader file " << path << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    std::string result;
    if(shaderCode)
    {
        std::ostringstream outStream;
        outStream << shaderCode.rdbuf();
        result = outStream.str();
        return result;
    }
    std::cerr << "Could not find " << path << std::endl;
    exit(EXIT_FAILURE);
}

GLuint ShaderProgram::compileShader(GLenum type, std::string path, std::string tag)
{
	DEBUG_METHOD("calling ShaderProgram::compileShader");
    // Create an empty shader:
	GLuint shader = glCreateShader(type);
	glCheckError("glCreateShader");

	// Read and specify the source code:
	std::string shaderSource = readShaderFile(path);
	const char* c_src = shaderSource.c_str();

	glShaderSource(shader, 1, (const char**)&c_src, NULL);
	glCheckError("glShaderSource");

	// Compile the shader:
	glCompileShader(shader);
	glCheckError("glCompileShader");

	// Check the compilation status:
	GLint success;

	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	glCheckError("glGetShaderiv");

	if (success)
	{
		return shader;
	}

	// Extract the length of the error message (including '\0'):
	GLint info_length = 0;

	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_length);
	glCheckError("glGetShaderiv");

	if (info_length > 1)
	{
		// Extract the message itself:
		char* info = new char[info_length];

		glGetShaderInfoLog(shader, info_length, NULL, info);
		glCheckError("glGetShaderInfoLog");

		std::cerr << "Error compiling shader " << tag << ": " << info << std::endl;
		delete[] info;
	}
	else
	{
		std::cerr << "No info log from the shader compiler" << std::endl;;
	}

	exit(EXIT_FAILURE);
}

void ShaderProgram::bindAttribute(int attributePosition, std::string variableName)
{
	DEBUG_METHOD("calling ShaderProgram::bindAttribute");
	glBindAttribLocation(programPtr, attributePosition, variableName.c_str());
	glCheckError("glBindAttribLocation");
}

GLuint ShaderProgram::getProgramPtr()
{
	DEBUG_METHOD("calling ShaderProgram::getProgramPtr");
    return programPtr;
}

GLint ShaderProgram::getUniformLoc(std::string name)
{
	DEBUG_METHOD("calling ShaderProgram::getUniformLoc");
	glUseProgram(programPtr);
	return glGetUniformLocation(programPtr, name.c_str());
}