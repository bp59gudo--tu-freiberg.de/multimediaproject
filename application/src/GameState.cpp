#include "../include/GameState.hpp"

GameState::State GameState::getState()
{
    DEBUG_METHOD("calling GameState::getState");
    if (debugging_enabled)
    {
        switch (state)
        {
        case State::menu:
            DEBUG("menu state");
            break;
        case State::paused:
            DEBUG("paused state");
            break;
        case State::running:
            DEBUG("running state");
            break;
        }
    }
    return state;
}

void GameState::setEvent(GameState::GameEvent event)
{
    DEBUG_METHOD("calling GameState::setEvent");
    switch (state)
    {
    case State::menu:
        switch (event)
        {
        case GameEvent::looseGame:
            break;
        case GameEvent::pause:
            break;
        case GameEvent::showOptions:
            eventShowOptions.fire(0);
            break;
        case GameEvent::winGame:
            break;
        case GameEvent::resizeWindow:
            eventResizeWindow.fire(*gameCamera);
            break;
        }
        break;

    case State::paused:
        switch (event)
        {
        case GameEvent::looseGame:
            break;
        case GameEvent::pause:
            state = State::running;
            eventPauseGame.fire(0);
            break;
        case GameEvent::showOptions:
            break;
        case GameEvent::winGame:
            break;
        case GameEvent::resizeWindow:
            eventResizeWindow.fire(*gameCamera);
            break;
        }
        break;

    case State::running:
        switch (event)
        {
        case GameEvent::looseGame:
            eventLooseGame.fire(*renderEngine);
            break;
        case GameEvent::pause:
            state = State::paused;
            eventPauseGame.fire(0);
            break;
        case GameEvent::showOptions:
            eventShowOptions.fire(0);
            break;
        case GameEvent::winGame:
            eventWinGame.fire(*renderEngine);
            break;
        case GameEvent::resizeWindow:
            eventResizeWindow.fire(*gameCamera);
            break;
        }
        break;
    }
}

void GameState::subscribeGameEvent(std::function<void(int)> func, GameState::GameEvent event)
{
    DEBUG_METHOD("calling GameState::subscribeGameEvent");
    switch (event)
    {
    case GameEvent::pause:
        eventPauseGame.subscribe(func);
        break;
    case GameEvent::showOptions:
        eventShowOptions.subscribe(func);
        break;
    }
}

void GameState::unsubscribeGameEvent(std::function<void(int)> func, GameState::GameEvent event)
{
    DEBUG_METHOD("calling GameState::unsubscribeGameEvent (Int)");
    switch (event)
    {
    case GameEvent::pause:
        eventPauseGame.unsubscribe(func);
        break;
    case GameEvent::showOptions:
        eventShowOptions.unsubscribe(func);
        break;
    }
}

void GameState::subscribeGameEvent(std::function<void(Camera &)> func, GameState::GameEvent event)
{
    DEBUG_METHOD("calling GameState::subscribeGameEvent");
    switch (event)
    {
    case GameEvent::resizeWindow:
        eventResizeWindow.subscribe(func);
        break;
    }
}

void GameState::unsubscribeGameEvent(std::function<void(Camera &)> func, GameState::GameEvent event)
{
    DEBUG_METHOD("calling GameState::unsubscribeGameEvent (Camera)");
    switch (event)
    {
    case GameEvent::resizeWindow:
        eventResizeWindow.unsubscribe(func);
        break;
    }
}

void GameState::subscribeGameEvent(std::function<void(RenderEngine &)> func, GameEvent event)
{
    DEBUG_METHOD("calling GameState::subscribeGameEvent (RenderEngine)");
    switch (event)
    {
    case GameEvent::looseGame:
        eventLooseGame.subscribe(func);
        break;
    case GameEvent::winGame:
        eventWinGame.subscribe(func);
        break;
    }
}

void GameState::unsubscribeGameEvent(std::function<void(RenderEngine &)> func, GameEvent event)
{
    DEBUG_METHOD("calling GameState::unsubscribeGameEvent (RenderEngine)");
    switch (event)
    {
    case GameEvent::looseGame:
        eventLooseGame.unsubscribe(func);
        break;
    case GameEvent::winGame:
        eventWinGame.unsubscribe(func);
        break;
    }
}

void GameState::initialize(Camera *_gameCamera, RenderEngine *_renderEngine)
{
    gameCamera = _gameCamera;
    renderEngine = _renderEngine;
}