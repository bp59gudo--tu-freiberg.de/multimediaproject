#include "../include/PhysicsEngine.hpp"

PhysicsEngine::PhysicsEngine()
{
	DEBUG_METHOD("calling PhysicsEngine::PhysicsEngine");
}

PhysicsEngine::~PhysicsEngine()
{
	DEBUG_METHOD("calling PhysicsEngine::~PhysicsEngine");
}

/**
 * @brief called each frame, updates the physic of all physical objects in the current game
 * 
 * @param timeDelta time passed from last frame
 */
void PhysicsEngine::update(float timeDelta)
{
	DEBUG_METHOD("calling PhysicsEngine::update");
    for (auto &elem : physicsObjects)
    {
        elem->updatePhysics(timeDelta, physicsObjects);
    }
    for (auto &elem : physicsObjects)
    {
        elem->resetCollision();
    }
}

/**
 * @brief adds a PhysicsObject to vector of all physical objects in the current game
 * 
 * @param obj Object to be added
 */
void PhysicsEngine::addPhysicsObject(PhysicsObject *obj)
{
	DEBUG_METHOD("calling PhysicsEngine::addPhysicsObject");
    physicsObjects.push_back(obj);
}

/**
 * @brief iterates through all PhysicObject in vector and deletes the one with the corresponding physicsID
 * 
 * @param physicsID ID of the PhysicObject
 */

void PhysicsEngine::removePhysicsObject(PhysicsObject *obj)
{
	DEBUG_METHOD("calling PhysicsEngine::removePhysicsObject");
    auto it = physicsObjects.begin();
    while (it != physicsObjects.end())
    {
        if (obj == *it)
        {
            physicsObjects.erase(it);
            break;
        }
        it++;
    }
}