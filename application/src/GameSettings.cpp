#include "../include/GameSettings.hpp"

GameSettings *GameSettings::gsInstance = nullptr;

//settings for Player
std::string GameSettings::playerName = "TeamFrischeFrikadellen";
bool GameSettings::fullscreen = true;
float GameSettings::mouseSensivity = 0.1f;
//settings for Bots:
int GameSettings::MaxMinAngle = 80;
int GameSettings::mintimeNextMovement = 2;
int GameSettings::maxtimeNextMovement = 10;
int GameSettings::amountGameActors = 20;
int GameSettings::amountItems = 50;
//settings for terrain:
float GameSettings::mountainEdge = 3.0f;
float GameSettings::terrainSize = 100.0f;
int GameSettings::vertexCount = 64;
//settings for heightgenerator:
float GameSettings::amplitude = 4.0f;
int GameSettings::octaves = 3;
float GameSettings::roughness = 0.3f;
//settings for gameactor:
float GameSettings::speed = 0.05f;

GameSettings *GameSettings::instance()
{
    DEBUG_METHOD("calling GameSettings::instance");
    //if there is no instance of the singleton yet -> create it
    if (!gsInstance)
        gsInstance = new GameSettings();
    return gsInstance;
}

GameSettings::GameSettings()
{
    DEBUG_METHOD("calling GameSettings::GameSettings");
}

GameSettings::~GameSettings()
{
    DEBUG_METHOD("calling GameSettings::~GameSettings");
    delete gsInstance;
}

void GameSettings::loadSettingsFile(std::string fileName)
{
    DEBUG_METHOD("calling GameSettings::loadSettingsFile");
    std::vector<std::string>* fileContent = getFileContent(fileName);

    DEBUG("loading game settings:");

    for(auto& line : *fileContent)
    {
        DEBUG(line);

        std::vector<std::string> lineContent = splitString(line, '=', ' ');

        if(lineContent.empty())
        {
            continue;
        }
        else if(lineContent[0] == "PLAYER_NAME")
        {
            playerName = lineContent[1];
        }
        else if(lineContent[0] == "FULLSCREEN")
        {
            if(lineContent[1] == "true")
            {
                fullscreen = true;
            }
            else if(lineContent[1] == "false")
            {
                fullscreen = false;
            }
            else
            {
                std::cout << "could not read 'FULLSCREEN' variable in options file. Please choose either 'true' or 'false'" << std::endl;
                exit(-1);
            }
        }
        else if(lineContent[0] == "MOUSE_SENSIVITY")
        {
            mouseSensivity = std::stof(lineContent[1]);
        }
        else if(lineContent[0] == "MAX_MIN_ANGLE")
        {
            MaxMinAngle = std::stoi(lineContent[1]);
        }
        else if(lineContent[0] == "MIN_TIME_NEXT_MOVEMENT")
        {
            mintimeNextMovement = std::stoi(lineContent[1]);
        }
        else if(lineContent[0] == "MAX_TIME_NEXT_MOVEMENT")
        {
            maxtimeNextMovement = std::stoi(lineContent[1]);
        }
         else if(lineContent[0] == "AMOUNT_GAMEACTORS")
        {
            amountGameActors = std::stoi(lineContent[1]);
        }
         else if(lineContent[0] == "AMOUNG_TIEMS")
        {
            amountItems = std::stoi(lineContent[1]);
        }
        else if(lineContent[0] == "MOUNTAIN_EDGE")
        {
            mountainEdge = std::stof(lineContent[1]);
        }
        else if(lineContent[0] == "TERRAIN_SIZE")
        {
            terrainSize = std::stof(lineContent[1]);
        }
        else if(lineContent[0] == "VERTEX_COUNT")
        {
            vertexCount = std::stoi(lineContent[1]);
        }
        else if(lineContent[0] == "AMPLITUDE")
        {
            amplitude = std::stof(lineContent[1]);
        }
        else if(lineContent[0] == "OCTAVES")
        {
            octaves = std::stoi(lineContent[1]);
        }
        else if(lineContent[0] == "ROUGHNESS")
        {
            roughness = std::stof(lineContent[1]);
        }
        else if(lineContent[0] == "BASE_SPEED")
        {
            speed = std::stof(lineContent[1]);
        }
    }

    delete fileContent;
}

    bool GameSettings::getFullscreen()
    {
        DEBUG_METHOD("calling GameSettings::getFullscreen");
        return fullscreen;
    }

    float GameSettings::getSensivity()
    {
        DEBUG_METHOD("calling GameSettings::getSensivity");
        return mouseSensivity;
    }
    
    int GameSettings::getMaxMinAngle()
    {
        DEBUG_METHOD("calling GameSettings::getMaxMinAngle");
        return MaxMinAngle;
    }

    int GameSettings::getMinTimeMovement()
    {
        DEBUG_METHOD("calling GameSettings::getMinTimeMovement");
        return mintimeNextMovement;
    }

    int GameSettings::getMaxTimeMovement()
    {
        DEBUG_METHOD("calling GameSettings::getMaxTimeMovement");
        return maxtimeNextMovement;
    }

    int GameSettings::getAmountGameactors()
    {
        DEBUG_METHOD("calling GameSettings::getMaxTimeMovement");
        return amountGameActors;
    }

    int GameSettings::getAmountItems()
    {
        DEBUG_METHOD("calling GameSettings::getAmountItems");
        return amountItems;
    }

    float GameSettings::getMountainEdge()
    {
        DEBUG_METHOD("calling GameSettings::getMountainEdge");
        return mountainEdge;
    }

    float GameSettings::getterrainSize()
    {
        DEBUG_METHOD("calling GameSettings::getterrainSize");
        return terrainSize;
    }

    int GameSettings::getVertexCount()
    {
        DEBUG_METHOD("calling GameSettings::getVertexCount");
        return vertexCount;
    }

    float GameSettings::getAmplitude()
    {
        DEBUG_METHOD("calling GameSettings::getAmplitude");
        return amplitude;
    }

    int GameSettings::getOctaves()
    {
        DEBUG_METHOD("calling GameSettings::getOctaves");
        return octaves;
    }

    float GameSettings::getRoughness()
    {
        DEBUG_METHOD("calling GameSettings::getRoughness");
        return roughness;
    }

    float GameSettings::getSpeed()
    {
        DEBUG_METHOD("calling GameSettings::getSpeed");
        return speed;
    }

    std::string GameSettings::getPlayerName()
    {
        DEBUG_METHOD("calling GameSettings::getPlayerName");
        return playerName;
    }