#include "Player.hpp"

void Player::updateCamera()
{
    DEBUG_METHOD("calling Player::updateCamera");
    glm::vec3 cameraPosition = glm::vec3(position.x, position.y + scale * 0.25f, position.z);
    camReference->setPosDir(cameraPosition, lookDirection);
}

void Player::changeDirection(float angle)
{
    DEBUG_METHOD("calling Player::changeDirection");
    float oldYawRad = glm::radians(yaw);
    yaw += angle;
    float lookAngleRad = glm::radians(yaw);

    rotate(oldYawRad - lookAngleRad);

    lookDirection.x = cos(lookAngleRad);
    lookDirection.z = sin(lookAngleRad);
}

void Player::render()
{
    DEBUG_METHOD("calling Player::render");
    if (!GameUserData::instance()->getCameraFlight())
    {
        updateCamera();
    }
}

void Player::setDirection(glm::vec3 dir)
{
    DEBUG_METHOD("calling Player::setDirection");
    lookDirection = dir;
}

glm::vec3 Player::getDirection()
{
    return lookDirection;
}