#include <iostream>
#include <thread>
#include <unistd.h>

#include "../include/Event.hpp"
#include "../include/GameUserData.hpp"
#include "../include/Camera.hpp"
#include "../include/util.hpp"
#include "../include/Model3D.hpp"
#include "../include/Item.hpp"
#include "../include/DefaultShader.hpp"
#include "../include/Font.hpp"
#include "../include/Player.hpp"
#include "../include/Skybox.hpp"

void errorCallback(int error, const char *description)
{
	std::cerr << "Error: " << description << ", " << error << std::endl;
	exit(EXIT_FAILURE);
}

void framebufferSizeCallback(GLFWwindow *window, int fb_width, int fb_height)
{
	glViewport(0, 0, fb_width, fb_height);
	glCheckError("glViewport");
}

void windowSizeCallback(GLFWwindow *window, int width, int height)
{
	GameUserData::instance()->resizeWindow(width, height);
}

void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		// specify mouse input (toggle when pressing escape)
		int mode = glfwGetInputMode(window, GLFW_CURSOR);
		if (mode == GLFW_CURSOR_DISABLED)
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		if (mode == GLFW_CURSOR_NORMAL)
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}
	else if (key == GLFW_KEY_Q && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
}

// see https://learnopengl.com/Getting-started/Camera for reference
void mouseCallback(GLFWwindow *window, double xpos, double ypos)
{
	if (debugging_enabled && GameUserData::instance()->getCameraFlight())
	{
		Camera *camReference = GameUserData::instance()->getGameCamera();

		//when first frame -> set mouse coords as lastX and lastY
		if (camReference->firstMouse)
		{
			camReference->cameraLastX = xpos;
			camReference->cameraLastY = ypos;
			camReference->firstMouse = false;
		}

		float xoffset = xpos - camReference->cameraLastX;
		float yoffset = camReference->cameraLastY - ypos;

		camReference->cameraLastX = xpos;
		camReference->cameraLastY = ypos;

		float sensitivity = GameSettings::instance()->getSensivity();
		xoffset *= sensitivity;
		yoffset *= sensitivity;

		camReference->yaw += xoffset;
		camReference->pitch += yoffset;

		//cap pitch at 90°
		if (camReference->pitch > 89.0f)
			camReference->pitch = 89.0f;
		if (camReference->pitch < -89.0f)
			camReference->pitch = -89.0f;

		glm::vec3 direction;
		direction.x = cos(glm::radians(camReference->yaw)) * cos(glm::radians(camReference->pitch));
		direction.y = sin(glm::radians(camReference->pitch));
		direction.z = sin(glm::radians(camReference->yaw)) * cos(glm::radians(camReference->pitch));
		camReference->front = glm::normalize(direction);
	}
	else
	{
		Player *gamePlayer = GameUserData::instance()->getPlayer();

		if (gamePlayer->firstMouse)
		{
			gamePlayer->lastX = xpos;
			gamePlayer->lastY = ypos;
			gamePlayer->firstMouse = false;
		}

		float xoffset = xpos - gamePlayer->lastX;
		gamePlayer->lastX = xpos;
		gamePlayer->lastY = ypos;

		xoffset *= GameSettings::instance()->getSensivity();

		gamePlayer->changeDirection(xoffset);
	}
}

int main()
{
	DEBUG("Debugging enabled");

	GameSettings::instance()->loadSettingsFile("../Settings.txt");

	glfwSetErrorCallback(errorCallback);
	checkError(!glfwInit(), "Failed to initialize GLFW.");

	int windowWidth = 800;
	int windowHeight = 600;

	//whole display
	GLFWmonitor *primary = glfwGetPrimaryMonitor();
	glfwGetMonitorWorkarea(primary, NULL, NULL, &windowWidth, &windowHeight);

	// We want at least OpenGL 4.1:
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

	// Enable forward-compatibility and use the core profile:
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Create a GLFW window:
	std::cout << "Creating window" << std::endl;

	GLFWwindow *window;
	if (GameSettings::instance()->getFullscreen())
	{
		window = glfwCreateWindow(windowWidth, windowHeight, "Birdies", primary, NULL);
	}
	else
	{
		window = glfwCreateWindow(windowWidth, windowHeight, "Birdies", NULL, NULL);
	}

	checkError(window == NULL, "Failed to create window.");

	// Make the OpenGL context of the window the current one:
	glfwMakeContextCurrent(window);

	// Loader function:
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

	// Try to swap on every screen update:
	glfwSwapInterval(1);

	// Enable back face culling
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	// Enable blending (for example for text rendering)
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// Enable Multisampling
	glfwWindowHint(GLFW_SAMPLES, 4);
	glEnable(GL_MULTISAMPLE);

	// Specify remaining callbacks:
	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
	glfwSetWindowSizeCallback(window, windowSizeCallback);
	glfwSetKeyCallback(window, keyCallback);
	glfwSetCursorPosCallback(window, mouseCallback);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_TRUE);

	// Initialize cursor mode
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Store a pointer to our user data inside the window (eventhough we don't really need it becouse we use GameUserData as 'global access' singleton)
	glfwSetWindowUserPointer(window, (void *)&GameUserData::gudInstance);

	DefaultShader textUIShader("fontUIVertex.glsl", "fontUIFragment.glsl");
	DefaultShader textShader("fontVertex.glsl", "fontFragment.glsl");
	Font arial("arial.ttf", windowHeight * 0.1f);

	// Specify the clear color:
	glClearColor(0.1, 0.1, 0.1, 1);
	glCheckError("glClearColor");

	// Clear the color buffer -> background color:
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glCheckError("glClear");

	arial.renderUIText("Loading... please wait", &textUIShader, -0.5f, 0.0f, 1.0f, glm::vec3(1.0f, 0.0f, 0.0f));

	//swap buffers
	glfwSwapBuffers(window);

	//begin testing!
	DefaultShader texShader;
	DefaultShader fakeskyboxshader{"fakeskyVertexShader.glsl", "fakeskyFragmentShader.glsl"};

	//create models for the player and the bots
	Model3D::Material defaultMaterial;
	Model3D birdModel = Model3D("bird.obj", "bird_blank_black.bmp", defaultMaterial);

	std::string playerName = GameSettings::instance()->getPlayerName();

	//create player and initialize the GameUserData (backbone of the game)
	Player *playerActor = new Player(playerName, &birdModel, &texShader, glm::vec3(50.0f, 0.0f, 50.0f), 0.3f);
	GameUserData::instance()->initialize(window, playerActor, windowWidth, windowHeight);

	//create model for items
	Model3D itemModel("Korn.obj", "Korn.bmp", defaultMaterial);

	//set main camera position
	Camera *camReference = GameUserData::instance()->getGameCamera();
	camReference->pos.x = 50.0f;
	camReference->pos.y = 50.0f;
	camReference->pos.z = 50.0f;

	int amountGameActors = GameSettings::instance()->getAmountGameactors();
	int amountItems = GameSettings::instance()->getAmountItems();
	std::vector<GameActor> gameactors;
	std::vector<Item> items;

	std::vector<std::string> *botNames = getFileContent("../data/botnames.txt");

	//tuples with x, z and radius of each placed object
	std::vector<std::tuple<float, float, float>> usedSpaces;
	int locMinSize = (int)(GameUserData::terrain->getSIZE() * 0.1f);
	int locMaxSize = (int)(GameUserData::terrain->getSIZE() * 0.9f);

	int iterator = 0;
	int errorCount = 0;
	bool overBorder = false;
	//set bot initial size to 0.3
	float objectSize = 0.3;
	while (iterator < amountGameActors)
	{
		//to compute x and z positions, we want to only use 90% of the terrain as the spawning field (see locMinSize and locMaxSize)
		float x = locMinSize + (rand() % (locMaxSize - locMinSize));
		float z = locMinSize + (rand() % (locMaxSize - locMinSize));

		bool hasEnoughSpace = true;

		for (auto &space : usedSpaces)
		{
			//2D collision check -> when the distance of own center and others center is closer than both radii (is that the correct plural?)
			if (glm::distance(glm::vec2(x, z), glm::vec2(std::get<0>(space), std::get<1>(space))) <= objectSize + std::get<2>(space))
			{
				hasEnoughSpace = false;
				break;
			}
		}

		if (hasEnoughSpace)
		{
			//get first name of the bot names and delete the entry in list
			std::string name = (*botNames)[0];
			removeFromVector<std::string>(*botNames, name);

			//get random r, g and b color values between 0 and 0.99
			glm::vec3 color = glm::vec3((rand() % 100) / 100.0f, (rand() % 100) / 100.0f, (rand() % 100) / 100.0f);

			//create a new bot and add it to gameactors
			gameactors.emplace_back(GameActor(name, &birdModel, &texShader, color, glm::vec3(x, 0.0f, z), objectSize));
			checkError(overBorder, "tried to place GameActor out of terrain boundaries");

			//add space, that is taken from this new bot to the usedSpaces vector
			usedSpaces.emplace_back(std::tuple<float, float, float>(x, z, objectSize + 2.0f));
			iterator++;
			errorCount = 0;
		}
		else
		{
			//when there was 100 times not enough space -> probably too many game actors -> end game
			errorCount++;
			checkError(errorCount >= 100, "terrain does not have enough space for " + std::to_string(amountGameActors) + " GameActors!");
		}
	}

	iterator = errorCount = 0;
	//set scale for items to 0.1
	objectSize = 0.1f;
	while (iterator < amountItems)
	{
		//to compute x and z positions, we want to only use 80% of the terrain as the spawning field (see locMinSize and locMaxSize)
		float x = locMinSize + (rand() % (locMaxSize - locMinSize));
		float z = locMinSize + (rand() % (locMaxSize - locMinSize));

		bool hasEnoughSpace = true;

		for (auto &space : usedSpaces)
		{
			//2D collision check -> when the distance of own center and others center is closer than both radii (is that the correct plural?)
			if (glm::distance(glm::vec2(x, z), glm::vec2(std::get<0>(space), std::get<1>(space))) <= objectSize + std::get<2>(space))
			{
				hasEnoughSpace = false;
				break;
			}
		}

		if (hasEnoughSpace)
		{
			bool overborder = false;

			//create new item and add it to items
			items.emplace_back(Item(&itemModel, &texShader, glm::vec3(x, GameUserData::terrain->getHeightOfTerrain(x, z, overborder) + 0.5f, z), objectSize));
			checkError(overBorder, "tried to place GameActor out of terrain boundaries");

			//add space that is used from the item to usedspaces
			usedSpaces.emplace_back(std::tuple<float, float, float>(x, z, objectSize + 0.5f));
			iterator++;
			errorCount = 0;
		}
		else
		{
			//when there was 100 times not enough space -> probably too many items -> end game
			errorCount++;
			checkError(errorCount >= 100, "terrain does not have enough space for " + std::to_string(amountItems) + " Items!");
		}
	}

	DEBUG("gameactors size:" << gameactors.size());
	DEBUG("items size:" << items.size());
	//adding all game objects to GameUserData:
	for (auto &elem : gameactors)
	{
		GameUserData::instance()->addGameObject(&elem);
		DEBUG(&elem << ": " << elem.getName());
	}
	
	//initialize the scoreboard to render the top 3 places
	GameUserData::instance()->renderEngine->initScoreboard(3);

	//add all created items to GameUserData
	for (auto &elem : items)
	{
		GameUserData::instance()->addGameObject(&elem);
	}

	//create fake skybox
	Model3D topskyplane = Model3D("../data/CubeMap/sky_plane_top.obj", "../data/CubeMap/beach/top.bmp", defaultMaterial);
	Model3D bottomskyplane = Model3D("../data/CubeMap/sky_plane_bottom.obj", "../data/CubeMap/beach/bottom.bmp", defaultMaterial);
	Model3D rightskyplane = Model3D("../data/CubeMap/sky_plane.obj", "../data/CubeMap/beach/right.bmp", defaultMaterial);
	Model3D leftskyplane = Model3D("../data/CubeMap/sky_plane.obj", "../data/CubeMap/beach/left.bmp", defaultMaterial);
	Model3D backskyplane = Model3D("../data/CubeMap/sky_plane.obj", "../data/CubeMap/beach/back.bmp", defaultMaterial);
	Model3D frontskyplane = Model3D("../data/CubeMap/sky_plane.obj", "../data/CubeMap/beach/front.bmp", defaultMaterial);

	///make skyscale smaller than minimum of gameactors so it's not on the scaleboard
	//plane size is 300x300:
	float skyscale = GameSettings::instance()->getterrainSize() / 150.0f;
	std::vector<Skybox> skyplanes;
	float mapcenter = GameSettings::instance()->getterrainSize() / 2;
	Skybox *topsky = new Skybox(&topskyplane, &fakeskyboxshader, glm::vec3(mapcenter, 300.0f * skyscale, mapcenter), skyscale);
	topsky->rotate(glm::radians(180.0f));
	skyplanes.emplace_back(*topsky);
	Skybox *bottomsky = new Skybox(&bottomskyplane, &fakeskyboxshader, glm::vec3(mapcenter, -300.0f * skyscale, mapcenter), skyscale);
	skyplanes.emplace_back(*bottomsky);
	Skybox *rightsky = new Skybox(&rightskyplane, &fakeskyboxshader, glm::vec3(300.0f * skyscale + mapcenter, 0.0f, mapcenter), skyscale);
	rightsky->rotate(glm::radians(-90.0f));
	skyplanes.emplace_back(*rightsky);
	Skybox *leftsky = new Skybox(&leftskyplane, &fakeskyboxshader, glm::vec3(-300.0f * skyscale + mapcenter, 0.0f, mapcenter), skyscale);
	leftsky->rotate(glm::radians(90.0f));
	skyplanes.emplace_back(*leftsky);
	Skybox *backsky = new Skybox(&backskyplane, &fakeskyboxshader, glm::vec3(mapcenter, 0.0f, 300.0f * skyscale + mapcenter), skyscale);
	backsky->rotate(glm::radians(180.0f));
	skyplanes.emplace_back(*backsky);
	Skybox *frontsky = new Skybox(&frontskyplane, &fakeskyboxshader, glm::vec3(mapcenter, 0.0f, -300.0f * skyscale + mapcenter), skyscale);
	skyplanes.emplace_back(*frontsky);

	//add fake skybox to GameUserData
	for (auto &elem : skyplanes)
	{
		GameUserData::instance()->addGameObject(&elem);
	}

	//randomly rotate gameactors and items once: (PI * 2 * 100(cause int) for 628)
	for (auto &elem : gameactors)
	{
		float random_rotate = float(rand() % 628) / 100.0f;
		elem.rotate(random_rotate);
	}
	for (auto &elem : items)
	{
		float random_rotate = float(rand() % 628) / 100.0f;
		elem.rotate(random_rotate);
	}
	
	//main loop
	while (!glfwWindowShouldClose(window))
	{
		//update game
		GameUserData::update();

		for (auto &elem : gameactors)
		{
			elem.move();
			elem.randomMovement();
		}

		for (auto &elem : items)
		{
			//rotate items
			elem.rotate(0.05f);
			//move up and down
			elem.translate(glm::vec3(0.0f, sin(glfwGetTime() * 5) * 0.1f, 0.0f));
		}

		//move player
		playerActor->move();

		//swap buffers
		glfwSwapBuffers(window);
		// React to the window manager's messages
		glfwPollEvents();
	}
	// Destroy the window:
	glfwDestroyWindow(window);

	// Terminate GLFW:
	glfwTerminate();

	delete botNames;
	delete playerActor;
	delete frontsky;
	delete backsky;
	delete topsky;
	delete bottomsky;
	delete rightsky;
	delete leftsky;

	items.clear();
	gameactors.clear();
	usedSpaces.clear();
	skyplanes.clear();

	return 0;
}