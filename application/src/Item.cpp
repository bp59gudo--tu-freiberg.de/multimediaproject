#include "../include/Item.hpp"

GameObject::ObjectType Item::getObjectType()
{
    DEBUG_METHOD("calling Item::getObjectType");
    return GameObject::ItemType;
}

//shader program is used before all Items are rendered!
void Item::render()
{
    DEBUG_METHOD("calling Item::render");

    //shader program is already used by RenderEngine

    //set uniforms
    glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(camReference->getProjectionMatrix()));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(camReference->getViewMatrix()));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelMatrix));
    glCheckError("glUniformMatrix4fv");

    //bind vao
    model->bindVAO();

    //activate texture slot 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, model->getTextureID());
    glCheckError("glBindTexture");

    glDrawArrays(GL_TRIANGLES, 0, model->getVertexCount());
    glCheckError("glDrawArrays");

    model->unbindVAO();
}

void Item::collide(PhysicsObject *other)
{
    DEBUG_METHOD("calling Item::collide");
    //all collision checks are done in the GameActor, so there is no need to do them again here
}

float Item::getSize()
{
    return scale;
}

void Item::rotate(float angle)
{
    rotateModel(angle);
}

ShaderProgram *Item::getShaderProgram()
{
    return program;
}

void Item::translate(glm::vec3 translation)
{
    translateModel(translation);
}