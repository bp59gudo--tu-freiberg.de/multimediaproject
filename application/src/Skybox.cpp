#include "../include/Skybox.hpp"

GameObject::ObjectType Skybox::getObjectType()
{
    DEBUG_METHOD("calling Skybox::getObjectType");
    return GameObject::ObjectType::SkyboxType;
}

void Skybox::render()
{
    DEBUG_METHOD("calling Skybox::render");

    //set uniforms
    glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(camReference->getProjectionMatrix()));
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(camReference->getViewMatrix()));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelMatrix));
    glCheckError("glUniformMatrix4fv");

    //bind vao
    model->bindVAO();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, model->getTextureID());
    glCheckError("glBindTexture");

    glDrawArrays(GL_TRIANGLES, 0, model->getVertexCount());
    glCheckError("glDrawArrays");

    model->unbindVAO();
}

void Skybox::collide(PhysicsObject *other)
{
    DEBUG_METHOD("calling Skybox::collide");
    //no collide function needed for skybox
}

float Skybox::getSize()
{
    return scale;
}

void Skybox::rotate(float angle)
{
    rotateModel(angle);
}

ShaderProgram *Skybox::getShaderProgram()
{
    return program;
}