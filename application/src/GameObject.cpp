#include "../include/GameObject.hpp"

Terrain* GameObject::terrainReference = nullptr;

void GameObject::skipCollision()
{
    DEBUG_METHOD("calling GameObject::skipCollision");
    skipCollisionCheck = true;
}

void GameObject::translateModel(glm::vec3& _trans)
{
    DEBUG_METHOD("calling GameObject::translateModel");
    modelMatrix = glm::translate(modelMatrix, _trans);

    //position is fourth column
    position = glm::vec3(modelMatrix[3]);
    center2D = glm::vec2(position.x, position.z);
}

void GameObject::rotateModel(float _angle_rad)
{
    DEBUG_METHOD("calling GameObject::rotateModel");
    modelMatrix = glm::rotate(modelMatrix, _angle_rad, glm::vec3(0, 1, 0));
}

void GameObject::scaleModel(float _scale)
{
    DEBUG_METHOD("calling GameObject::scaleModel");
    modelMatrix = glm::scale(modelMatrix, glm::vec3(_scale, _scale, _scale));
    collisionRadius = scale;
}

void GameObject::initialize(Terrain* terrain)
{
    DEBUG_METHOD("calling GameObject::initialize");
    terrainReference = terrain;
}

glm::vec3 GameObject::getPosition()
{
    DEBUG_METHOD("calling GameObject::getPosition");
    return position;
}

float GameObject::getScale()
{
    DEBUG_METHOD("calling GameObject::getScale");

    return scale;
}