#include "../include/Font.hpp"

Camera *Font::camRef = nullptr;

Font::Font(std::string fontPath, int fontSizeHeight, int fontSizeWidth)
{
    DEBUG_METHOD("calling Font::Font");

    //initialize lib if it does not exist yet
    FT_Error error;
    error = FT_Init_FreeType(&ftLib);
    checkError(error, "Freetype error: library initialization");

    //check if ttf file exists
    if (!std::experimental::filesystem::exists(fontPath))
    {
        std::string fontPathOrig = fontPath;
        fontPath = "../data/fonts/" + fontPath;
        if (!std::experimental::filesystem::exists(fontPath))
        {
            std::cerr << "Couldn't find file " << fontPathOrig << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    //create font and set font size
    error = FT_New_Face(ftLib, fontPath.c_str(), 0, &face);
    checkError(error, "Freetype error: loading font " + fontPath);
    FT_Set_Pixel_Sizes(face, fontSizeWidth, fontSizeHeight);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // disable byte-alignment restriction (usually textures need to have a 4-byte alignment)

    //create ASCII glyphs
    for (unsigned char c = 0; c < 128; c++)
    {
        // load character glyph
        checkError(FT_Load_Char(face, c, FT_LOAD_RENDER), "Freetype error: Failed to load glyph");

        // generate texture
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            face->glyph->bitmap.width,
            face->glyph->bitmap.rows,
            0,
            GL_RED,
            GL_UNSIGNED_BYTE,
            face->glyph->bitmap.buffer);
        glCheckError("Freetype: glTexImage2D");

        // set texture options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glCheckError("Freetype: glTexParameteri");

        // now store character for later use
        Character character = {
            texture,
            glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
            glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
            static_cast<unsigned int>(face->glyph->advance.x)};
        characters.insert(std::pair<char, Character>(c, character));
    }

    //free font resources
    FT_Done_Face(face);
    FT_Done_FreeType(ftLib);

    //create and bind vao and vbo
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    //reserve memory
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, NULL, GL_DYNAMIC_DRAW); //2D quad requires 6 vertices (each 4 floats)
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);

    //unbind buffers
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

Font::~Font()
{
    DEBUG_METHOD("calling Font::~Font");
}

void Font::renderUIText(std::string text, DefaultShader *shader, float x, float y, float scale, glm::vec3 color)
{
    DEBUG_METHOD("calling Font::renderUIText");

    //scale is pretty high by default -> divide by 1000
    scale /= 1000;

    //disable depth test when rendering text on screen -> is never occluded
    glDisable(GL_DEPTH_TEST);

    //use shader program, texture and bind vao
    glUseProgram(shader->getProgramPtr());
    glUniform3f(shader->getUniformLoc("uniform_textColor"), color.x, color.y, color.z);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(vao);

    // iterate through all characters
    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++)
    {
        //select character from map
        Character ch = characters[*c];

        float xPos = x + ch.bearing.x * scale;
        float yPos = y - (ch.size.y - ch.bearing.y) * scale;

        float w = ch.size.x * scale;
        float h = ch.size.y * scale;
        // update vbo for each character
        float vertices[6][4] = {//x pos, y pos, z texCoord u, w texCoord v
                                {xPos, yPos + h, 0.0f, 0.0f},
                                {xPos, yPos, 0.0f, 1.0f},
                                {xPos + w, yPos, 1.0f, 1.0f},

                                {xPos, yPos + h, 0.0f, 0.0f},
                                {xPos + w, yPos, 1.0f, 1.0f},
                                {xPos + w, yPos + h, 1.0f, 0.0f}};
        // render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, ch.textureID);
        // update content of vbo memory
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // render quad
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        x += (ch.advance >> 6) * scale; // bitshift by 6 to get value in pixels (2^6 = 64)
    }
    //unbind buffers
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);

    //reenable depth test
    glEnable(GL_DEPTH_TEST);
}

void Font::renderPositionText(std::string text, DefaultShader *shader, glm::vec3 worldPosition, float scale, glm::vec3 color)
{
    DEBUG_METHOD("calling Font::renderPositionText");

    //scale is pretty high by default -> divide by 1000
    scale /= 1000;

    //compute length of the text
    std::string::const_iterator c;
    //beginning of the text (in local coordinates)
    glm::vec3 origin = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 endWorldPos = origin;
    for (c = text.begin(); c != text.end(); c++)
    {
        //select character from map
        Character ch = characters[*c];

        float xPos = endWorldPos.x + ch.bearing.x * scale;

        // advance cursors for next glyph (note that advance is number of 1/64 pixels)
        endWorldPos.x += (ch.advance >> 6) * scale; // bitshift by 6 to get value in pixels (2^6 = 64)
    }

    glm::vec3 cameraPos = camRef->getPosition();
    //the look direction vector is camera - object world position
    glm::vec2 dirVec = glm::normalize(glm::vec2(cameraPos.x, cameraPos.z) - glm::vec2(worldPosition.x, worldPosition.z));

    //to compute the angle, we use the dot product of vec2(0, 1) (which is the default view direction for text objects)
    // and the vec2 of the look direction -> is always dirVec.y
    float angle = acos(dirVec.y);
    if (dirVec.x < 0.0f)
    {
        angle = -angle;
    }

    // shortened y-axis rotation (z values are always 0)
    float oldX = endWorldPos.x;
    endWorldPos.x = cos(angle) * oldX;
    endWorldPos.z = -sin(angle) * oldX;

    // rotated end of the word by angle and subtract half of that vector from original render position to render it at the right position
    glm::vec3 rotationPosition = worldPosition - 0.5f * endWorldPos;

    // use program and set the uniforms for color, projection and view
    glUseProgram(shader->getProgramPtr());
    glUniform3f(shader->getUniformLoc("uniform_textColor"), color.x, color.y, color.z);
    glUniformMatrix4fv(shader->getUniformLoc("uniform_projection"), 1, GL_FALSE, glm::value_ptr(camRef->getProjectionMatrix()));
    glUniformMatrix4fv(shader->getUniformLoc("uniform_view"), 1, GL_FALSE, glm::value_ptr(camRef->getViewMatrix()));

    // set model matrix as identity matrix
    glm::mat4 modelMat(1.0f);

    // rotate model matrix by previously calculated angle and set the position
    modelMat = glm::rotate(modelMat, angle, glm::vec3(0.0f, 1.0f, 0.0f));
    modelMat[3][0] = rotationPosition.x;
    modelMat[3][1] = rotationPosition.y;
    modelMat[3][2] = rotationPosition.z;

    // set uniform for the model matrix
    glUniformMatrix4fv(shader->getUniformLoc("uniform_model"), 1, GL_FALSE, glm::value_ptr(modelMat));
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(vao);

    // iterate through all characters
    for (c = text.begin(); c != text.end(); c++)
    {
        //select character from map
        Character ch = characters[*c];

        float xPos = origin.x + ch.bearing.x * scale;
        float yPos = origin.y - (ch.size.y - ch.bearing.y) * scale;

        float w = ch.size.x * scale;
        float h = ch.size.y * scale;
        // update vbo for each character
        float vertices[6][4] = {//x pos, y pos, z texCoord u, w texCoord v
                                {xPos, yPos + h, 0.0f, 0.0f},
                                {xPos, yPos, 0.0f, 1.0f},
                                {xPos + w, yPos, 1.0f, 1.0f},

                                {xPos, yPos + h, 0.0f, 0.0f},
                                {xPos + w, yPos, 1.0f, 1.0f},
                                {xPos + w, yPos + h, 1.0f, 0.0f}};
        // render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, ch.textureID);
        // update content of vbo memory
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // render quad
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        origin.x += (ch.advance >> 6) * scale; // bitshift by 6 to get value in pixels (2^6 = 64)
    }
    //unbind buffers
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Font::initialize(Camera *gameCamera)
{
    DEBUG_METHOD("calling Font::initialize");
    camRef = gameCamera;
}