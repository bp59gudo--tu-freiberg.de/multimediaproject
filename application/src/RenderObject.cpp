#include "../include/RenderObject.hpp"

Camera* RenderObject::camReference = nullptr;

void RenderObject::initialize(Camera* gameCamera)
{
    DEBUG_METHOD("calling RenderObject::initialize");
    camReference = gameCamera;
}