#include "../include/DefaultShader.hpp"

void DefaultShader::initProgram()
{
    DEBUG_METHOD("calling DefaultShader::initProgram");

    //compile shaders
    DEBUG("compiling vertex shader: " + vertexPath);
    GLuint vertexShader = compileShader(GL_VERTEX_SHADER, vertexPath, "vertex shader");
    DEBUG("compiling fragment shader: " + fragmentPath);
    GLuint fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragmentPath, "fragment shader");

    //create shader program
    GLuint shaderProgram = glCreateProgram();

    //attach shaders
    glAttachShader(shaderProgram, vertexShader);
    glCheckError("glAttachShader [vertex]");
    glAttachShader(shaderProgram, fragmentShader);
    glCheckError("glAttachShader [fragment]");

    //link shader program
    glLinkProgram(shaderProgram);
    glCheckError("glLinkProgram");

    //detach and delete shaders
    glDetachShader(shaderProgram, vertexShader);
    glCheckError("glDetachShader [vertex]");
    glDetachShader(shaderProgram, fragmentShader);
    glCheckError("glDetachShader [fragment]");
    glDeleteShader(vertexShader);
    glCheckError("glDeleteShader [vertex]");
    glDeleteShader(fragmentShader);
    glCheckError("glDeleteShader [fragment]");

    // Check the compilation status:
	GLint success;

	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	glCheckError("glGetProgramiv");

    //if succesfull -> set programPtr and return
	if (success)
	{
		glUseProgram(shaderProgram);
        glCheckError("glUseProgram");

        programPtr = shaderProgram;

        glReleaseShaderCompiler();
        glCheckError("glReleaseShaderCompiler");

        DEBUG("using shader program succesfully");
        return;
	}

	// Extract the length of the error message (including '\0'):
	GLint info_length = 0;

	glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &info_length);
	glCheckError("glGetProgramiv");

	if (info_length > 1)
	{
		// Extract the message itself:
		char* info = new char[info_length];

		glGetProgramInfoLog(shaderProgram, info_length, NULL, info);
		glCheckError("glGetProgramInfoLog");

		DEBUG("Error linking program: " + std::string(info));
		delete[] info;
	}
	else
	{
		DEBUG("No info log from the program linker");
	}

	exit(EXIT_FAILURE);
}

DefaultShader::DefaultShader(std::string _vertexPath, std::string _fragmentPath)
{
    DEBUG_METHOD("calling DefaultShader::DefaultShader");
    vertexPath = _vertexPath;
    fragmentPath = _fragmentPath;
    initProgram();
}

DefaultShader::~DefaultShader()
{
    DEBUG_METHOD("calling DefaultShader::~DefaultShader");
}