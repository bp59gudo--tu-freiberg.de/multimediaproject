#include "../include/HeightGenerator.hpp"

HeightGenerator::HeightGenerator()
{
	DEBUG_METHOD("calling HeightGenerator::HeightGenerator ()");
	std::random_device random;
	std::uniform_int_distribution<long> uniform_dist(0, 10000000000);
	HeightGenerator::seed = uniform_dist(random);
}

//only works with POSITIVE gridX and gridZ values!
HeightGenerator::HeightGenerator(int gridX, int gridZ, int vertexCount, int seed)
{
	DEBUG_METHOD("calling HeightGenerator::HeightGenerator (int gridX, int gridZ, int vertexCount, int seed)");
	HeightGenerator::seed = seed;
	xOffset = gridX * (vertexCount - 1);
	zOffset = gridZ * (vertexCount - 1);
}

float HeightGenerator::interpolate(float a, float b, float blend)
{
	DEBUG_METHOD("calling HeightGenerator::interpolate");
	double theta = blend * M_PI;
	float f = (float)(1.0f - cos(theta)) * 0.5f;
	return a * (1.0f - f) + b * f;
}

float HeightGenerator::getNoise(int x, int z)
{
	DEBUG_METHOD("calling HeightGenerator::getNoise");
	srand(x * 49632 + z * 325176 + seed);
	float nextFloat = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
	return nextFloat * 2.0f - 1.0f;
}

float HeightGenerator::getSmoothNoise(int x, int z)
{
	DEBUG_METHOD("calling HeightGenerator::getSmoothNoise");
	float corners = (getNoise(x - 1, z - 1) + getNoise(x + 1, z - 1) + getNoise(x - 1, z + 1) + getNoise(x + 1, z + 1)) / 16.0f;
	float sides = (getNoise(x - 1, z) + getNoise(x + 1, z) + getNoise(x, z - 1) + getNoise(x, z + 1)) / 8.0f;
	float center = getNoise(x, z) / 4.0f;
	return corners + sides + center;
}

float HeightGenerator::getInterpolatedNoise(float x, float z)
{
	DEBUG_METHOD("calling HeightGenerator::getInterpolatedNoise");
	int intX = (int)x;
	int intZ = (int)z;
	float fracX = x - intX;
	float fracZ = z - intZ;

	float v1 = getSmoothNoise(intX, intZ);
	float v2 = getSmoothNoise(intX + 1, intZ);
	float v3 = getSmoothNoise(intX, intZ + 1);
	float v4 = getSmoothNoise(intX + 1, intZ + 1);
	float i1 = interpolate(v1, v2, fracX);
	float i2 = interpolate(v3, v4, fracX);
	return interpolate(i1, i2, fracZ);
}

float HeightGenerator::generateHeight(int x, int z)
{
	DEBUG_METHOD("calling HeightGenerator::generateHeight");
	float total = 0;
	float d = (float)pow(2, octaves - 1);
	for (int i = 0; i < octaves; i++)
	{
		float freq = (float)(pow(2, i) / d);
		float amp = (float)pow(roughness, i) * amplitude;
		total += getInterpolatedNoise((x + xOffset) * freq, (z + zOffset) * freq) * amp;
	}
	return total;
}


float HeightGenerator::getMaxAmplitude()
{
	DEBUG_METHOD("calling HeightGenerator::getMaxAmplitude");
	return amplitude;
}