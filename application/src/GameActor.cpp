#include "../include/GameActor.hpp"

std::vector<GameActor *> *GameActor::otherActors = nullptr;

GameObject::ObjectType GameActor::getObjectType()
{
    DEBUG_METHOD("calling GameActor::getObjectType (" << name << ")");
    return GameObject::ObjectType::GameActorType;
}

void GameActor::move(glm::vec2 deltaMovement)
{
    DEBUG_METHOD("calling GameActor::move");

    bool overBorder = false;

    // compute the delta movement
    glm::vec3 movement3D = glm::vec3(deltaMovement.x, terrainReference->getHeightOfTerrain(position.x + deltaMovement.x, position.z + deltaMovement.y, overBorder) + this->scale - position.y, deltaMovement.y);
    //check if movement (+ a little buffer) would take actor out of map boundaries
    terrainReference->getHeightOfTerrain(position.x + deltaMovement.x + 3.0f * lookDirection.x, position.z + deltaMovement.y + 3.0f * lookDirection.z, overBorder);
    
    //do this for all bots (the collision check is done in RandomMovement)
    if (this != (GameActor *)GameUserData::instance()->getPlayer())
    {
        translateModel(movement3D);
    }
    //when the player is not over border -> move
    else if (!overBorder)
    {
        translateModel(movement3D);
    }
}

void GameActor::changeDirection(float angle)
{
    DEBUG_METHOD("calling GameActor::changeDirection");
    //for this frequent rotation, we write the rotation matrix directly in hope to make it a little bit faster
    lookDirection.x = cos(angle) * lookDirection.x + sin(angle) * lookDirection.z;
    lookDirection.z = -sin(angle) * lookDirection.x + cos(angle) * lookDirection.z;

    //keep look direction always normalized
    lookDirection = glm::normalize(lookDirection);
}

void GameActor::collide(PhysicsObject *other)
{
    DEBUG_METHOD("calling GameActor::collide");

    //skip collision check if it is already done by another actor
    if (skipCollisionCheck)
    {
        skipCollisionCheck = false;
        return;
    }

    GameObject *collider = static_cast<GameObject *>(other);
    //skip the collision check of the other GameObject, since we do it here
    collider->skipCollision();
    //for some reason, we need to call collider->getObjectType() twice or it won't work...
    GameObject::ObjectType type = collider->getObjectType();
    switch (collider->getObjectType())
    {
    case GameObject::ObjectType::GameActorType:
    {
        GameActor *otherActor = static_cast<GameActor *>(collider);
        //when the other actor has a lower scale (size/ points) -> remove the other actor from game, else remove this actor
        if (otherActor->scale < this->scale)
        {
            //compute size bonus (is multiplied on current scale)
            float sizeBonus = std::max(1.3f * otherActor->scale, 1.1f);
            scale *= sizeBonus;
            scaleModel(sizeBonus);
            GameUserData::instance()->removeGameObject(otherActor);
            
            //if the other actor is the player -> fire 'loose game' event
            if (otherActor == (GameActor *)GameUserData::instance()->getPlayer())
            {
                GameUserData::instance()->getGameState()->setEvent(GameState::GameEvent::looseGame);
            }
        }
        else
        {
            float sizeBonus = std::max(1.3f * otherActor->scale, 1.1f);
            otherActor->scale *= sizeBonus;
            otherActor->scaleModel(sizeBonus);
            GameUserData::instance()->removeGameObject(this);

            //if this actor is the player -> fire 'loose game' event
            if (this == (GameActor *)GameUserData::instance()->getPlayer())
            {
                GameUserData::instance()->getGameState()->setEvent(GameState::GameEvent::looseGame);
            }
        }
        break;
    }

    case GameObject::ObjectType::ItemType:
    {
        //when consuming items -> add a fixed amount of points
        scale += 0.05;
        //scaleModel(scale / (scale - 0.05));
        GameUserData::instance()->removeGameObject(collider);
        break;
    }

    case GameObject::ObjectType::SkyboxType:
    {
        //checkError(true, "It should not be possible for game actors to collide with a sky box!"); TODO
        break;
    }
    }

    GameUserData::instance()->updateScoreboard();
}

float GameActor::getSize()
{
    return scale;
}

std::string GameActor::getName()
{
    return name;
}

void GameActor::setColor(float red, float green, float blue)
{
    newColorR = red;
    newColorG = green;
    newColorB = blue;
}

void GameActor::rotate(float angle)
{
    rotateModel(angle);
}

//shader program is used before all gameActors are rendered!
void GameActor::render()
{
    DEBUG_METHOD("calling GameActor::render");

    //shader is already used by RenderEngine

    //set uniforms
    glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(camReference->getProjectionMatrix()));
    glUniform3fv(colorLoc, 1, &color[0]);
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(camReference->getViewMatrix()));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelMatrix));
    glCheckError("glUniformMatrix4fv");

    //bind vao
    model->bindVAO();

    //activate texture
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, model->getTextureID());
    glCheckError("glBindTexture");

    //draw triangles
    glDrawArrays(GL_TRIANGLES, 0, model->getVertexCount());
    glCheckError("glDrawArrays");
    model->unbindVAO();
}

void GameActor::move()
{
    DEBUG_METHOD("calling GameActor::move");

    //speedmodifier from 1 / scale to let them all run on the same base speed
    float speedmodifier = 1.0f / scale;

    //when debugging, make player faster for quicker debugging (it's not cheating)
    if (debugging_enabled)
    {
        if (this == (GameActor *)GameUserData::instance()->getPlayer())
        {
            speedmodifier *= 2.0f;
        }
    }
    //make bigger actors slower than small actors
    speedmodifier += 1.0f / scale;

    //move actor
    this->move(glm::vec2(1.0f, 0.0f) * GameSettings::instance()->getSpeed() * speedmodifier);
}

void GameActor::randomMovement()
{
    DEBUG_METHOD("calling GameActor::randomMovement");

    //set a new look direction if the former one has been reached ->
    if ((currentDirection <= goalOflookDirection && negativeSteering) || (currentDirection >= goalOflookDirection && !negativeSteering))
    {
        //random goal of next look direction
        int MaxMinAngle = GameSettings::instance()->getMaxMinAngle(); //in degrees
        int angle = MaxMinAngle * 2;

        //compute random angle
        goalOflookDirection = (float(rand() % angle) - float(MaxMinAngle));
        if (goalOflookDirection <= 0)
        {
            negativeSteering = true;
        }
        else
            negativeSteering = false;

        timeNextMovement = float(rand() % (maxtimeNextMovement - mintimeNextMovement) + mintimeNextMovement);
        //reset current Direction:
        currentDirection = 0.0f;
    }

    //check if game actor is overborder (border of x-axis) and mirror its look direction
    if (this->getPosition().x >= GameSettings::instance()->getterrainSize() - 4 || this->getPosition().x < 4)
    {
        //look direction is normalized -> rotation matrix simplified
        glm::vec3 oldlookDir = lookDirection;
        //negate x-axis to bounce from border
        lookDirection.x = lookDirection.x * -1;
        float mirroredAngle = glm::acos(glm::dot(oldlookDir, lookDirection));
        rotate(mirroredAngle);
    }
    //check if game actor is overborder (border of z-axis) and mirror its look direction
    else if (this->getPosition().z >= GameSettings::instance()->getterrainSize() - 4 || this->getPosition().z < 4)
    {
        //look direction is normalized -> rotation matrix simplified
        glm::vec3 oldlookDir = lookDirection;
        //negate z-axis to bounce from border
        lookDirection.z = lookDirection.z * -1;
        float mirroredAngle = glm::acos(glm::dot(oldlookDir, lookDirection));
        rotate(mirroredAngle);
    }
    //actor is not over border
    else
    {
        float angle = goalOflookDirection / timeNextMovement;
        float oldYawRad = glm::radians(currentDirection);
        currentDirection += angle;
        float lookAngleRad = glm::radians(currentDirection);
        this->changeDirection(angle);
        rotate(oldYawRad - lookAngleRad);
    }
}

// first attempts of implementing simple AI. Does not work yet, so it is not used
void GameActor::findEatable()
{
    DEBUG_METHOD("calling GameActor::findEatable");

    float rotAngle = 0.0f;
    float rotAngleMax = glm::radians(45.0f);
    float range = 15.0f;

    std::vector<GameObject *> actorsOfInterest;
    actorsOfInterest.clear();

    for (auto &obj : *otherActors)
    {
        glm::vec3 objRelativePos = obj->getPosition() - position;
        //make if statement as big as possible to really only get the objects of interest
        if (glm::distance(obj->getPosition(), position) <= range + obj->getScale() && obj != this && obj->getScale() < scale)
        {
            actorsOfInterest.emplace_back(obj);
        }
    }

    if (!actorsOfInterest.empty())
    {
        //sort by most points
        std::sort(actorsOfInterest.begin(), actorsOfInterest.end(), [](GameObject *first, GameObject *second) {
            return first->getScale() > second->getScale();
        });

        //ERROR
        for (auto it = actorsOfInterest.begin(); it != actorsOfInterest.end(); it++)
        {
            glm::vec2 oldlookDir = glm::normalize(glm::vec2(lookDirection.x, lookDirection.z));
            auto otherPos = (*it)->getPosition();
            glm::vec2 newLookDir = glm::normalize(glm::vec2(otherPos.x, otherPos.z) - glm::vec2(position.x, position.z));
            float rotAngle = 0.01f * glm::acos(glm::dot(oldlookDir, newLookDir));

            if (rotAngle <= rotAngleMax)
            {
                if (name == "PiepChiep")
                {
                    DEBUG("i am: ");
                    printVec3(position);
                    DEBUG("i love: ");
                    auto tmp = (*it)->getPosition();
                    printVec3(tmp);
                    DEBUG("angle: " << rotAngle);
                }

                rotate(rotAngle);
                return;
            }
        }
        randomMovement();
    }
    else
    {
        randomMovement();
    }
}

void GameActor::setOtherObjects(std::vector<GameActor *> *others)
{
    otherActors = others;
}

ShaderProgram *GameActor::getShaderProgram()
{
    return program;
}