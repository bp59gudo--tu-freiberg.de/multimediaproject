#include "../include/RenderEngine.hpp"

void RenderEngine::looseScreen()
{
    DEBUG_METHOD("calling RenderEngine::looseScreen");

    std::cout << std::endl
              << "You Died" << std::endl
              << std::endl
              << "Scoreboard:" << std::endl
              << std::endl;
    for (auto &s : scoreboard)
    {
        //using fprintf stdout for easier formatting
        fprintf(stdout, "%-30s%.2f\n", s->getName().c_str(), s->getScale() * 100.0f);
    }

    glfwSetWindowShouldClose(window, GLFW_TRUE);
    return;

    //we tried to render a loosing screen, but it did not work so we need to end the game
    glClearColor(0.1, 0.1, 0.1, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glfwSwapBuffers(window);
    while (!glfwWindowShouldClose(window))
    {
        // Clear the color buffer -> background color:
        glClearColor(0.1, 0.1, 0.1, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glCheckError("glClear");

        text->renderUIText("You Died", textUIShader, -0.67f, 0.0f, 3.0f, glm::vec3(1.0f, 0.0f, 0.0f));
        text->renderUIText("Press enter to play again, press q to quit", textUIShader, -0.95f, -0.3f, 1.0f, glm::vec3(1.0f, 0.0f, 0.0f));

        if (glfwGetKey(window, GLFW_KEY_ENTER) == GLFW_PRESS)
        {
            //TODO restart game
            DEBUG("devs should add the possibility to restart the game now...");
        }

        //swap buffers
        glfwSwapBuffers(window);
        // React to the window manager's messages
        glfwPollEvents();
    }
}

void RenderEngine::winScreen()
{
    DEBUG_METHOD("calling RenderEngine::winScreen");

    std::cout << "You won!" << std::endl;
    glfwSetWindowShouldClose(window, GLFW_TRUE);
}

RenderEngine::RenderEngine(Terrain *mapTerrain, int windowHeight)
{
    DEBUG_METHOD("calling RenderEngine::RenderEngine");
    renderActorList = std::vector<GameActor *>();
    renderItemList = std::vector<Item *>();
    fakesky = std::vector<Skybox *>();
    terrain = mapTerrain;

    window = GameUserData::instance()->getWindowPointer();
    text = new Font("arial.ttf", windowHeight * 0.1f);
    scoreboardLength = -1;
    scoreboard = std::vector<GameActor *>();
    DEBUG(windowHeight);
    textShader = new DefaultShader("fontVertex.glsl", "fontFragment.glsl");
    textUIShader = new DefaultShader("fontUIVertex.glsl", "fontUIFragment.glsl");
    GameActor::setOtherObjects(&renderActorList);

    std::function<void(RenderEngine &)> looseMethod = &RenderEngine::looseScreen;
    GameUserData::instance()->getGameState()->subscribeGameEvent(looseMethod, GameState::GameEvent::looseGame);
    std::function<void(RenderEngine &)> winMethod = &RenderEngine::winScreen;
    GameUserData::instance()->getGameState()->subscribeGameEvent(winMethod, GameState::GameEvent::winGame);
}

RenderEngine::~RenderEngine()
{
    DEBUG_METHOD("calling RenderEngine::~RenderEngine");
    delete text;
    delete textShader;
    delete textUIShader;
}

void RenderEngine::update(float timeDelta)
{
    DEBUG_METHOD("calling RenderEngine::update");

    // Clear the color buffer -> background color:
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glCheckError("glClear");

    //get reference to player
    GameActor *player = (GameActor *)GameUserData::instance()->getPlayer();

    //render game actors
    glUseProgram(renderActorList[0]->getShaderProgram()->getProgramPtr());
    glCheckError("glUseProgram [GameActor]");
    glEnableVertexAttribArray(ATTRIB_POSITION);
    glEnableVertexAttribArray(ATTRIB_COLOR);
    glEnableVertexAttribArray(ATTRIB_NORMAL);
    glEnableVertexAttribArray(ATTRIB_TEX_COORDS);
    glCheckError("glEnableVertexAttribArray [GameActor]");
    for (auto &elem : renderActorList)
    {
        elem->render();
    }
    glDisableVertexAttribArray(ATTRIB_POSITION);
    glDisableVertexAttribArray(ATTRIB_COLOR);
    glDisableVertexAttribArray(ATTRIB_NORMAL);
    glDisableVertexAttribArray(ATTRIB_TEX_COORDS);
    glCheckError("glDisableVertexAttribArray [GameActor]");

    //render items
    glUseProgram(renderItemList[0]->getShaderProgram()->getProgramPtr());
    glCheckError("glUseProgram [Item]");
    glEnableVertexAttribArray(ATTRIB_POSITION);
    glEnableVertexAttribArray(ATTRIB_COLOR);
    glEnableVertexAttribArray(ATTRIB_NORMAL);
    glEnableVertexAttribArray(ATTRIB_TEX_COORDS);
    glCheckError("glEnableVertexAttribArray [Item]");
    for (auto &elem : renderItemList)
    {
        elem->render();
    }
    glDisableVertexAttribArray(ATTRIB_POSITION);
    glDisableVertexAttribArray(ATTRIB_COLOR);
    glDisableVertexAttribArray(ATTRIB_NORMAL);
    glDisableVertexAttribArray(ATTRIB_TEX_COORDS);
    glCheckError("glDisableVertexAttribArray [Item]");

    //render terrain
    terrain->render();

    //render fake skybox cause normal one doesn't work
    glUseProgram(fakesky[0]->getShaderProgram()->getProgramPtr());
    glCheckError("glUseProgram");
    glEnableVertexAttribArray(ATTRIB_POSITION);
    glEnableVertexAttribArray(ATTRIB_COLOR);
    glEnableVertexAttribArray(ATTRIB_NORMAL);
    glEnableVertexAttribArray(ATTRIB_TEX_COORDS);
    glCheckError("glEnableVertexAttribArray");
    for (auto &elem : fakesky)
    {
        elem->render();
    }
    glDisableVertexAttribArray(ATTRIB_POSITION);
    glDisableVertexAttribArray(ATTRIB_COLOR);
    glDisableVertexAttribArray(ATTRIB_NORMAL);
    glDisableVertexAttribArray(ATTRIB_TEX_COORDS);
    glCheckError("glDisableVertexAttribArray");

    //render names
    for (auto &elem : renderActorList)
    {
        glm::vec3 textPosition = elem->getPosition();
        //render name above actor
        textPosition.y += elem->getSize() + 1.0f;
        glm::vec3 nameColor = glm::vec3(0.0f, 1.0f, 0.0f);
        if (elem->getSize() >= player->getSize())
        {
            nameColor = glm::vec3(1.0f, 0.0f, 0.0f);
        }
        text->renderPositionText(elem->getName(), textShader, textPosition, 3.0f, nameColor);
    }

    //render scoreboard
    //if there are not enough actors -> set scoreboardLength to min(scoreboardLength, renderActorList.size())
    scoreboardLength = scoreboardLength < renderActorList.size() ? scoreboardLength : renderActorList.size();
    checkError(scoreboardLength < 0, "scoreboard not initialized!");

    //if there is just one actor left, it is the player
    if (renderActorList.size() == 1)
    {
        GameUserData::instance()->getGameState()->setEvent(GameState::GameEvent::winGame);
    }

    int counter = 0;
    float scoreBoardBaseColor = 1.0f / scoreboardLength;
    for (auto &elem : scoreboard)
    {
        if (counter >= scoreboardLength)
        {
            break;
        }

        GameActor *currentActor = elem;
        int score = (int)(elem->getSize() * 100.0f);
        
        std::string scoreText = currentActor->getName() + ": " + std::to_string(score);
        const glm::vec3 drawColor = glm::vec3(61 / 255.0f, 1 / 255.0f, 16 / 255.0f);
        if(counter == 0)
        {
            scoreText = "1st: " + scoreText;
        }
        else if(counter == 1)
        {
            scoreText = "2nd: " + scoreText;
        }
        else if(counter == 2)
        {
            scoreText = "3rd: " + scoreText;
        }

        text->renderUIText(scoreText, textUIShader, -1.0f, 0.9f - counter * 0.1f, 0.8 - counter * 0.05f, drawColor);
        counter++;
    }

    //render player score on bottom left
    text->renderUIText("Score: " + std::to_string((int)(player->getSize() * 100.0f)), textUIShader, -1.0f, -0.95f, 0.8f, glm::vec3(0.6f, 0.1f, 0.1f));
}

void RenderEngine::addGameObject(GameObject *obj)
{
    DEBUG_METHOD("calling RenderEngine::addGameObject");

    switch (obj->getObjectType())
    {
    case GameObject::ObjectType::GameActorType:
    {
        GameActor *actorObj = static_cast<GameActor *>(obj);
        renderActorList.emplace_back(actorObj);
        scoreboard.emplace_back(actorObj);
        break;
    }
    case GameObject::ObjectType::ItemType:
    {
        renderItemList.emplace_back(static_cast<Item *>(obj));
        break;
    }
    case GameObject::ObjectType::SkyboxType:
    {
        fakesky.emplace_back(static_cast<Skybox *>(obj));
        break;
    }
    }
}

void RenderEngine::removeGameObject(GameObject *obj)
{
    DEBUG_METHOD("calling RenderEngine::removeGameObject");

    switch (obj->getObjectType())
    {
    case GameObject::ObjectType::GameActorType:
    {
        GameActor *actor = reinterpret_cast<GameActor *>(obj);
        auto raIterator = renderActorList.begin();
        while (raIterator != renderActorList.end())
        {
            if (actor == *raIterator)
            {
                renderActorList.erase(raIterator);
                break;
            }
            raIterator++;
        }

        //delete entry from scoreboard
        auto scoreboardIterator = scoreboard.begin();
        while (scoreboardIterator != scoreboard.end())
        {
            if (*scoreboardIterator == actor)
            {
                scoreboard.erase(scoreboardIterator);
                break;
            }
            scoreboardIterator++;
        }
        break;
    }
    case GameObject::ObjectType::ItemType:
    {
        Item *item = reinterpret_cast<Item *>(obj);
        auto riIterator = renderItemList.begin();
        while (riIterator != renderItemList.end())
        {
            if (item == *riIterator)
            {
                renderItemList.erase(riIterator);
                break;
            }
            riIterator++;
        }
        break;
    }
    }
}

void RenderEngine::initScoreboard(int length)
{
    DEBUG_METHOD("calling RenderEngine::initScoreboard");
    checkError(length > renderActorList.size(), "Scoreboard can't contain more elements than objects in the scene!");
    scoreboardLength = length;
    scoreboard.shrink_to_fit();

    //for some reason sorting by (end, begin) does not seem to work, so we have to use a lambda function...
    std::sort(scoreboard.begin(), scoreboard.end(), [](GameActor *first, GameActor *second) {
        return first->getSize() > second->getSize();
    });
}

void RenderEngine::updateScoreboard()
{
    DEBUG_METHOD("calling RenderEngine::updateScoreboard");
    //update scoreboard

    //for some reason sorting by (end, begin) does not seem to work, so we have to use a lambda function...
    std::sort(scoreboard.begin(), scoreboard.end(), [](GameActor *first, GameActor *second) {
        return first->getSize() > second->getSize();
    });
}