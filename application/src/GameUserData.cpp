#include "../include/GameUserData.hpp"

GameUserData *GameUserData::gudInstance = nullptr;
RenderEngine *GameUserData::renderEngine = nullptr;
PhysicsEngine *GameUserData::physicsEngine = nullptr;
GameState *GameUserData::gameState = nullptr;
Terrain *GameUserData::terrain = nullptr;
GLFWwindow *GameUserData::window = nullptr;
Camera *GameUserData::gameCamera = nullptr;
DefaultShader *GameUserData::terrainShader = nullptr;
Player *GameUserData::player = nullptr;


std::vector<GameObject *> GameUserData::toDelete = std::vector<GameObject *>();

int GameUserData::windowWidth = 0;
int GameUserData::windowHeight = 0;
double GameUserData::dTime = 0.0f;
double GameUserData::lastTime = 0.0f;

bool GameUserData::cameraFlight = false;

GameUserData *GameUserData::instance()
{
    DEBUG_METHOD("calling GameUserData::instance");
    //return singleton instance -> if it does not exist yet, create it
    if (!gudInstance)
        gudInstance = new GameUserData();
    return gudInstance;
}

void GameUserData::removeGameObjects()
{
    DEBUG_METHOD("calling GameUserData::removeGameObjects");
    for (auto &obj : toDelete)
    {
        renderEngine->removeGameObject(obj);
        physicsEngine->removePhysicsObject(obj);
    }
    toDelete.clear();
}

GameUserData::GameUserData()
{
    DEBUG_METHOD("calling GameUserData::GameUserData");
    std::cout << "Don't forget to initialize GameUserData::instance in the main function" << std::endl;
}

GameUserData::~GameUserData()
{
    DEBUG_METHOD("calling GameUserData::~GameUserData");
    delete gameState;
    delete gameCamera;
    delete terrainShader;
    delete terrain;
    delete renderEngine;
    delete physicsEngine;

    delete gudInstance;
}

void GameUserData::initialize(GLFWwindow *wndPtr, Player *_player, int wndWidth, int wndHeight)
{
    DEBUG_METHOD("calling GameUserData::initialize");
    window = wndPtr;
    gameState = new GameState(GameState::State::running);
    gameCamera = new Camera(wndWidth, wndHeight);
    Font::initialize(gameCamera);
    RenderObject::initialize(gameCamera);

    terrainShader = new DefaultShader("TerrainVertex.glsl", "TerrainFragment.glsl");
    DefaultShader textShader("fontVertex.glsl", "fontFragment.glsl");

    // Obtain the internal size of the framebuffer:
    int fbWidth, fbHeight;
    glfwGetFramebufferSize(window, &fbWidth, &fbHeight);

    // Align the viewport to the framebuffer:
    glViewport(0, 0, fbWidth, fbHeight);
    glCheckError("glViewport");

    // Specify the clear color:
    glClearColor(0.1, 0.1, 0.1, 1);
    glCheckError("glClearColor");

    // Enable the depth test:
    glEnable(GL_DEPTH_TEST);
    glCheckError("glEnable [GL_DEPTH_TEST]");

    int terrainx = 0;
    int terrainz = 0;
    terrain = new Terrain(gameCamera, terrainShader, glm::vec3(terrainx, 0.0f, terrainz));

    player = _player;

    GameObject::initialize(terrain);
    renderEngine = new RenderEngine(terrain, wndHeight);
    physicsEngine = new PhysicsEngine();
    windowWidth = wndWidth;
    windowHeight = wndHeight;
    lastTime = glfwGetTime();
    //delta time
    dTime = 0.0;

    gameState->initialize(gameCamera, renderEngine);
    addGameObject((GameActor *)(player));
    std::cout << "GameUserData::instance initialized succesfully" << std::endl;
}

double GameUserData::getDeltaTime()
{
    DEBUG_METHOD("calling GameUserData::getDeltaTime");
    return dTime;
}

void GameUserData::update()
{
    DEBUG_METHOD("calling GameUserData::update");
    //cap at 60 fps
    while (glfwGetTime() < lastTime + 1.0 / 60.0f)
    {
        sleep(0.01f);
    }

    double newTime = glfwGetTime();
    dTime = newTime - lastTime;
    lastTime = newTime;

    //update camera
    gameCamera->update(dTime, window);
    //update physics engine in thread
    std::thread physicsUpdate(&PhysicsEngine::update, *physicsEngine, (float)dTime);
    //update render engine
    renderEngine->update(dTime);

    physicsUpdate.join();

    //delete objects at the end of the frame
    removeGameObjects();
}

GameState *GameUserData::getGameState()
{
    DEBUG_METHOD("calling GameUserData::getGameState");
    return gameState;
}

Terrain *GameUserData::getTerrain()
{
    DEBUG_METHOD("calling GameUserData::getTerrain");
    return terrain;
}

Camera *GameUserData::getGameCamera()
{
    DEBUG_METHOD("calling GameUserData::getGameCamera");
    return gameCamera;
}

GLFWwindow *GameUserData::getWindowPointer()
{
    DEBUG_METHOD("calling GameUserData::getWindowPointer");
    return window;
}

int GameUserData::getWindowWidth()
{
    DEBUG_METHOD("calling GameUserData::getWindowWidth");
    return windowWidth;
}

int GameUserData::getWindowHeight()
{
    DEBUG_METHOD("calling GameUserData::getWindowHeight");
    return windowHeight;
}

void GameUserData::resizeWindow(int width, int height)
{
    DEBUG_METHOD("calling GameUserData::resizeWindow");
    windowWidth = width;
    windowHeight = height;
    getGameState()->setEvent(GameState::GameEvent::resizeWindow);
}

void GameUserData::addGameObject(GameObject *obj)
{
    DEBUG_METHOD("calling GameUserData::addGameObject");
    renderEngine->addGameObject(obj);
    physicsEngine->addPhysicsObject(obj);
}

void GameUserData::removeGameObject(GameObject *obj)
{
    DEBUG_METHOD("calling GameUserData::removeGameObject");

    toDelete.emplace_back(obj);
}

void GameUserData::updateScoreboard()
{
    DEBUG_METHOD("calling GameUserData::updateScoreboard");
    renderEngine->updateScoreboard();
}

Player *GameUserData::getPlayer()
{
    DEBUG_METHOD("calling GameUserData::getPlayer");
    return player;
}

bool GameUserData::getCameraFlight()
{
    DEBUG_METHOD("calling GameUserData::getCameraFlight");
    return cameraFlight;
}

void GameUserData::toggleCameraFlight()
{
    DEBUG_METHOD("calling GameUserData::toggleCameraFlight");
    cameraFlight = !cameraFlight;
}

void GameUserData::setCameraFlight(bool camFlight)
{
    DEBUG_METHOD("calling GameUserData::setCameraFlight");
    cameraFlight = camFlight;
}