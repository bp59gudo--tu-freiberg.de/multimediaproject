#include "../include/Camera.hpp"

//based on http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
Camera::Camera(int windowWidth, int windowHeight, glm::vec3 position, glm::vec3 target,
               float _fov, float _nearPlane, float _farPlane)
{
    DEBUG_METHOD("calling Camera::Camera");
    //create projection matrix
    fov = _fov;
    nearPlane = _nearPlane;
    farPlane = _farPlane;
    float aspectRatio = (float)windowWidth / (float)windowHeight;
    projectionMatrix = glm::perspective(glm::radians(fov), aspectRatio, nearPlane, farPlane);

    //set pitch, yaw -> look direction
    pitch = 0.0f;
    yaw = -90.0f;
    glm::vec3 direction;
    direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    direction.y = sin(glm::radians(pitch));
    direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    front = normalize(direction);

    //set camera position and the front and upwards direction
    pos = position;
    up = glm::vec3(0.0f, 1.0f, 0.0f);

    //create view matrix
    viewMatrix = glm::lookAt(
        pos,
        pos + front,
        up);

    //initialize variables for mouse control
    cameraLastX = windowWidth / 2;
    cameraLastY = windowHeight / 2;
    firstMouse = true;

    //subscribe to ResizeWindow event
    std::function<void(Camera &)> setWindowSizeMethod = &Camera::setWindowSize;
    GameUserData::instance()->getGameState()->subscribeGameEvent(setWindowSizeMethod, GameState::GameEvent::resizeWindow);
}

Camera::~Camera()
{
    DEBUG_METHOD("calling Camera::~Camera");
    GameUserData::instance()->getGameState()->unsubscribeGameEvent(&Camera::setWindowSize, GameState::GameEvent::resizeWindow);
}

glm::mat4 Camera::getViewMatrix()
{
    DEBUG_METHOD("calling Camera::getViewMatrix");
    return viewMatrix;
}

glm::mat4 Camera::getProjectionMatrix()
{
    DEBUG_METHOD("calling Camera::getProjectionMatrix");
    return projectionMatrix;
}

void Camera::setWindowSize()
{
    DEBUG_METHOD("calling Camera::setWindowSize");
    int windowWidth = GameUserData::instance()->getWindowWidth();
    int windowHeight = GameUserData::instance()->getWindowHeight();
    projectionMatrix = glm::perspective(glm::radians(fov), (float)windowWidth / (float)windowHeight, nearPlane, farPlane);
}

void Camera::setPosDir(glm::vec3 position, glm::vec3 direction)
{
    DEBUG_METHOD("calling Camera::setPosDir");
    pos = position;
    front = direction;
}

void Camera::moveCamera(glm::vec3 &transform)
{
    DEBUG_METHOD("calling Camera::moveCamera");
    viewMatrix = glm::translate(viewMatrix, transform);
}

void Camera::rotateCamera(float angle)
{
    DEBUG_METHOD("calling Camera::rotateCamera");
    viewMatrix = glm::rotate(viewMatrix, angle, glm::vec3(0.0, 1.0, 0.0));
}

void Camera::update(float deltaTime, GLFWwindow *window)
{
    DEBUG_METHOD("calling Camera::update");

    //for flying camera
    if (debugging_enabled)
    {
        if (GameUserData::instance()->getCameraFlight())
        {
            const float cameraBaseSpeed = 7.5f;
            float cameraSpeed = cameraBaseSpeed * deltaTime;
            if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
                cameraSpeed *= 3.0f;

            if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
                pos += cameraSpeed * front;
            if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
                pos -= cameraSpeed * front;
            if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
                pos -= glm::normalize(glm::cross(front, up)) * cameraSpeed;
            if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
                pos += glm::normalize(glm::cross(front, up)) * cameraSpeed;
        }

        if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        {
            GameUserData::instance()->setCameraFlight(true);
        }
        else
        {
            GameUserData::instance()->setCameraFlight(false);
        }
    }

    //set view matrix to look in front direction
    viewMatrix = glm::lookAt(
        pos,
        pos + front,
        up);
}

glm::vec3 Camera::getPosition()
{
    DEBUG_METHOD("calling Camera::getPosition");
    return pos;
}

glm::vec3 Camera::getViewDir()
{
    DEBUG_METHOD("calling Camera::getViewDir");
    return front;
}