#version 410
//for rendering text at a position

in vec2 f_texCoords;
out vec4 color;

uniform vec3 uniform_textColor;
uniform sampler2D textSampler;

void main()
{    
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(textSampler, f_texCoords).r);
    color = vec4(uniform_textColor, 1.0) * sampled;
} 