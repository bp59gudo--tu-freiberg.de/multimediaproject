#version 410
//for rendering text at a world position

layout (location = 0) in vec4 v_vertex; // <vec2 pos, vec2 tex>

out vec2 f_texCoords;

uniform mat4 uniform_projection;
uniform mat4 uniform_view;
uniform mat4 uniform_model;

void main()
{
    f_texCoords = v_vertex.zw;
    gl_Position = uniform_projection * uniform_view * uniform_model * vec4(v_vertex.xy, 0.0, 1.0);
}  
