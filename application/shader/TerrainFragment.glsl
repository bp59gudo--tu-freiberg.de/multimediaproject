#version 410 core
//for terrain

in lowp vec4 f_color;
in vec3 f_normal;
in vec3 f_position;

uniform float uniform_size;
uniform vec2 uniform_resolution;

// Ouput data
out vec4 fragColor;

void main()
{
    float ambientStrenght = 0.1f;
    vec3 lightPos = vec3(20.0f, 100.0f, 50.0f);
    vec3 lightColor = vec3(0, 255, 255);

    vec3 lightDir = normalize(lightPos - f_position);
    vec3 reflectDir = reflect(-lightDir, f_normal);
    vec3 viewDir = normalize(-f_position);

    vec3 iAmb = vec3(0.5);
    vec3 iDiff = max(vec3(0.6) * dot(f_normal, lightDir), 0.1f);
    int k = 50;
    vec3 iSpec = vec3(0.6) * pow(max(dot(reflectDir, viewDir), 0.0f), k);

    fragColor = f_color * vec4(iAmb + iDiff + iSpec, 1.0f);
}