#version 410
// used for our skybox

layout(location = 0) in vec4 v_position;
layout(location = 1) in vec4 v_color;
layout(location = 2) in vec4 v_normal;
layout(location = 3) in vec2 v_texCoords;

uniform mat4 uniform_model;
uniform mat4 uniform_view;
uniform mat4 uniform_projection;

out vec4 f_color;
out vec3 f_normal;
out vec3 f_position;
out vec2 f_texCoords;

void main()
{
    f_color = v_color;
    f_position = vec3(uniform_model * v_position).xyz;
	f_normal = normalize(uniform_model * v_normal).xyz;
    f_texCoords = v_texCoords;
    gl_Position = uniform_projection * uniform_view * uniform_model * v_position;
}