#version 410
//for the game actors and items

precision mediump float;

in lowp vec2 f_texCoords;
in lowp vec4 f_color;
in vec3 f_normal;
in vec3 f_position;

uniform float uniform_size;
uniform vec2 uniform_resolution;
uniform vec3 uniform_color;
uniform sampler2D textureSampler;

out vec4 fragColor;

void main()
{
    float ambientStrenght = 0.1f;
    vec3 lightPos = vec3(0.0f, -1.0f, 0.0f);
    vec3 lightColor = vec3(0, 255, 255);

    vec3 lightDir = normalize(lightPos - f_position);
    vec3 reflectDir = reflect(-lightDir, f_normal);
    vec3 viewDir = normalize(-f_position);

    vec3 iAmb = vec3(0.5);
    vec3 iDiff = max(vec3(0.6) * dot(f_normal, lightDir), 0.1f);
    int k = 50;
    vec3 iSpec = vec3(0.6) * pow(max(dot(reflectDir, viewDir), 0.0f), k);

    vec4 tempFragColor = texture(textureSampler, f_texCoords) * f_color * vec4(iAmb + iDiff + iSpec, 1.0f);

    if(tempFragColor.r == 0.0f && tempFragColor.g == 0.0f && tempFragColor.b == 0.0f)
    {
        fragColor = vec4(uniform_color, 1.0f) * vec4(iAmb + iDiff + iSpec, 1.0f);
    }else{
    fragColor = texture(textureSampler, f_texCoords) * f_color * vec4(iAmb + iDiff + iSpec, 1.0f);
    }
}