#version 410 core
//for terrain

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec4 v_color;
layout(location = 2) in vec3 v_normal;

//uniform mat4 uniform_mvp;
uniform mat4 uniform_model;
uniform mat4 uniform_view;
uniform mat4 uniform_projection;

out vec4 f_color;
out vec3 f_normal;
out vec3 f_position;

void main(){
    //gourod
    f_color = v_color;
    f_position = vec3(uniform_model * vec4(v_position, 1.0f)).xyz;
	f_normal = normalize(uniform_model * vec4(v_normal, 1.0f)).xyz;
    gl_Position = uniform_projection * uniform_view * uniform_model * vec4(v_position, 1.0);
}