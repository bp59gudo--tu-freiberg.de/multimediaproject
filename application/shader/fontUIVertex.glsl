#version 410
//for rendering text on screen

layout (location = 0) in vec4 v_vertex; // <vec2 pos, vec2 tex>

out vec2 f_texCoords;

void main()
{
    gl_Position = vec4(v_vertex.xy, 0.0, 1.0);
    f_texCoords = v_vertex.zw;
}  