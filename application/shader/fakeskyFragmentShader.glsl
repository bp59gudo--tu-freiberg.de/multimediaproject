#version 410
// used for our skybox
precision mediump float;

in lowp vec2 f_texCoords;
in lowp vec4 f_color;
in vec3 f_normal;
in vec3 f_position;

uniform float uniform_size;
uniform vec2 uniform_resolution;
uniform vec3 uniform_color;
uniform sampler2D textureSampler;

out vec4 fragColor;

void main()
{

    vec3 iAmb = vec3(1);

    vec4 tempFragColor = texture(textureSampler, f_texCoords) * f_color * vec4(iAmb, 1.0f);

    if(tempFragColor.r == 0.0f && tempFragColor.g == 0.0f && tempFragColor.b == 0.0f)
    {
        fragColor = vec4(uniform_color, 1.0f) * vec4(iAmb, 1.0f);
    }else{
    fragColor = texture(textureSampler, f_texCoords) * f_color * vec4(iAmb, 1.0f);
    }
}