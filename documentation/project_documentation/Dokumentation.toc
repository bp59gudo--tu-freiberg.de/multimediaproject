\contentsline {section}{\numberline {1}Einleitung}{2}{section.1}%
\contentsline {section}{\numberline {2}Benutzerhandbuch}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Erstellen des Programms}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Spielanleitung}{3}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Spieleinstellungen}{4}{subsection.2.3}%
\contentsline {section}{\numberline {3}Codedokumentation}{5}{section.3}%
\contentsline {subsection}{\numberline {3.1}Projektstruktur}{5}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Genutzte Bibliotheken und Quellen}{5}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}\"Ubersicht \"uber die Programmstruktur}{6}{subsubsection.3.1.2}%
\contentsline {section}{\numberline {4}Projektdokumentation}{8}{section.4}%
\contentsline {subsection}{\numberline {4.1}Arbeitsweise}{8}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Arbeitsaufteilung}{11}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Entwicklungsumgebung}{11}{subsection.4.3}%
\contentsline {section}{\numberline {5}Anmerkungen}{12}{section.5}%
\contentsline {subsection}{\numberline {5.1}Probleme}{12}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}M\"ogliche Weiterentwicklung}{14}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Verworfene Programmteile}{15}{subsection.5.3}%
\contentsline {subsubsection}{\numberline {5.3.1}Metaballs}{15}{subsubsection.5.3.1}%
\contentsline {subsubsection}{\numberline {5.3.2}KI f\"ur die Bots}{15}{subsubsection.5.3.2}%
\contentsline {subsubsection}{\numberline {5.3.3}3rd Person Kamera}{15}{subsubsection.5.3.3}%
\contentsline {section}{\numberline {6}Anhang}{16}{section.6}%
\contentsline {subsection}{\numberline {6.1}UML-Diagramme}{16}{subsection.6.1}%
