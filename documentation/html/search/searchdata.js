var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvwxyz~",
  1: "_cdefghimprstv",
  2: "bcdefghimprstu",
  3: "abcdefghiklmoprstuw~",
  4: "abcdefghilmnoprstuvwxyz",
  5: "bop",
  6: "_gos",
  7: "gilmoprsw",
  8: "gkmop",
  9: "_abdem"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Macros"
};

