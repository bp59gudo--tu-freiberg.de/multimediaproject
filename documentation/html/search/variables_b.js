var searchData=
[
  ['name_1951',['name',['../classGameActor.html#a1dc58dda01a4f337ee14d44840a815c3',1,'GameActor']]],
  ['nearplane_1952',['nearPlane',['../classCamera.html#a78d8722a95e4f7770d79c950bda2bfdc',1,'Camera']]],
  ['negativesteering_1953',['negativeSteering',['../classGameActor.html#a6428ac6435fd1ad0d5241e1a80b6f37b',1,'GameActor']]],
  ['newcolorb_1954',['newColorB',['../classGameActor.html#a75a851c3750360415459fe40c5a3af18',1,'GameActor']]],
  ['newcolorg_1955',['newColorG',['../classGameActor.html#a23d6fd9d36f3308096de2be99737588e',1,'GameActor']]],
  ['newcolorr_1956',['newColorR',['../classGameActor.html#a2d3437d6753df6df6b42330a89ac7213',1,'GameActor']]],
  ['normal_1957',['normal',['../structModel3D_1_1VertexData.html#a04b161124fd58c0bbe3057406c4ddfe9',1,'Model3D::VertexData']]],
  ['normalentry_1958',['normalEntry',['../unionModel3D_1_1__obj__entry__t__.html#a7824ed6d128b105b624aa56350aa80d8',1,'Model3D::_obj_entry_t_']]],
  ['normalindex_1959',['normalIndex',['../structModel3D_1_1____obj__face__index__triple__t____.html#a62df78bd2a1014fbd0d9f89b6ec6bcd8',1,'Model3D::__obj_face_index_triple_t__']]]
];
