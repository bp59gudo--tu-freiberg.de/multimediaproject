# Setup, Hilfestellungen und wichtige Links

## Nötige Programme und Bibliotheken
- [OpenGL-Bibliothek] -> TODO
- Doxygen -> `sudo apt install doxygen`

## Sinnvolle VS Code Extensions
- C/C++
- CMake
- Doxygen Documentation Generator
- Shader language support for VS Code

## Doxygen
Codedokumentation: https://www.doxygen.nl/manual/docblocks.html
