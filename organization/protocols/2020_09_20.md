# Protokoll vom 20.09. - 12:00 Uhr

#### Wochenübersicht
- Brian
    - -> Prüfung
- Patrick
    - -> UI
    - -> Texturen
    - -> Skybox
    - -> Quaternionen für Rotation verwenden (oder bessere Möglichkeit finden)
- Lisa
    - -> UML Diagramm auf den neuesten Stand bringen
- Baldur
    - -> Terrain sollte nicht mehr von GameObject erben
    - -> Kamera über dem Spieler
    - -> KI
    - -> Namen für Bots generieren und darüber anzeigen lassen
    - -> Grund für segmentation fault finden
    - -> UML Diagramm auf den neuesten Stand bringen
