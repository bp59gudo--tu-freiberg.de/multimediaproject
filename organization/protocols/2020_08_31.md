# Protokoll vom 24.08. - 15:00 Uhr

#### Planung
  - Camera von RenderEngine zu GameUserData (beim rendern müssen alle GameObjects auf die view- und projection matrix der Kamera zugreifen)
  - Bewegungsmethoden und evtl. `GameObject Anchor` -> damit man die Kamera über dem Spieler verankern kann (?) für Camera. Anchor nicht in Camera, sondern bei Player die Kamera übergeben.
  - Speichermanagement -> Alles was in den Heap kann nicht mit `std::vector<OBJECT>` anlegen sondern als pointer vector mit `std::vector<OBJECT*>`
  - Klassen für ShaderProgram nötig
  - ProgramPtr von Model3D zu GameObject, da die ModelMatrix bei jedem GameObject unterschiedlich ist, aber viele GameObjects den VAO des gleichen Model3Ds nutzen können

#### Wochenübersicht
  - Brian
      - -> objektorientierte Implementierung von gl_calls aus der Übung
  - Patrick
      - -> GameActor (so weit es geht)
  - Lisa
      - -> UI Recherche
  - Baldur
      - -> RenderEngine::render -> im UML Diagramm public machen
      - -> ProgramShader Klasse
      - -> Model3D

#### Organisation:
  - interne Deadline festlegen -> 27. September
