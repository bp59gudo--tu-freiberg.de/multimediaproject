# meatballs - a 3D variant of 'Agar.io'
(see original game: https://agar.io/), final project for multimedia

## current planning and organization
can be seen on https://kanban.xsitepool.tu-freiberg.de/s/g9kz7oIVw#

## Necessary packages and libraries for building MEATBALLS:
- cmake (`sudo apt install cmake`)
- glfw
- freetype (use `installFreetype.sh`) to download and build the library automatically
